use crate::{csr::Privilege, memory::Memory};

//                        76543210
pub const FLAG_V: u8 = 0b0000000_1_;
pub const FLAG_R: u8 = 0b000000_1_0;
pub const FLAG_W: u8 = 0b00000_1_00;
pub const FLAG_X: u8 = 0b0000_1_000;
pub const FLAG_U: u8 = 0b000_1_0000;
pub const FLAG_G: u8 = 0b00_1_00000;
pub const FLAG_A: u8 = 0b0_1_000000;
pub const FLAG_D: u8 = 0b_1_0000000;

pub enum TranslationError {
    PhysicalFault,
    PageFault,
}
impl From<crate::memory::MemoryLoadError> for TranslationError {
    fn from(_value: crate::memory::MemoryLoadError) -> Self {
        Self::PhysicalFault
    }
}
impl From<crate::memory::MemoryStoreError> for TranslationError {
    fn from(_value: crate::memory::MemoryStoreError) -> Self {
        Self::PhysicalFault
    }
}
impl From<crate::memory::MemoryAtomicError> for TranslationError {
    fn from(_value: crate::memory::MemoryAtomicError) -> Self {
        Self::PhysicalFault
    }
}

/// Page-Based 39-bit Virtual-Memory System.
pub struct Translation<'a, M: Memory> {
    pub mem: &'a mut M,
    pub privilege: Privilege,
    pub satp: u64,
}
impl<'a, M: Memory> Translation<'a, M> {
    pub fn translate(&self, addr: u64) -> Result<u64, TranslationError>
    where
        M: Memory,
    {
        if self.privilege == Privilege::Machine {
            return Ok(addr);
        }

        let satp = self.satp;

        let mode = satp >> 60;
        let vpn_layers = match mode {
            0 => {
                // Raw
                return Ok(addr);
            }

            8 => {
                // Sv39 (max 3 layers)
                3
            }
            9 => {
                // Sv48 (max 4 layers)
                4
            }
            10 => {
                // Sv57 (max 5 layers)
                5
            }

            _ => {
                // Reserved
                unimplemented!("Invalid mode")
            }
        };

        let vpns = [
            ((addr >> 12) & 0b1_11111111) << 2,
            ((addr >> 21) & 0b1_11111111) << 2,
            ((addr >> 30) & 0b1_11111111) << 2,
            (addr >> 39) & 0b1_11111111,
            (addr >> 48) & 0b1_11111111,
        ];

        let mut page_ptr = (satp & ((1 << 44) - 1)) << 12;
        for i in (0..vpn_layers).rev() {
            let ppe = self
                .mem
                .load64(page_ptr | vpns[i])
                .map_err(|_| TranslationError::PhysicalFault)?;

            let ppe_flags = ppe as u8;

            if ppe_flags & FLAG_V == 0 {
                unimplemented!("Page fault: invalid ppe");
            }
            if !(ppe_flags & FLAG_R) | (ppe_flags & FLAG_W) != 0 {
                unimplemented!("Page fault: write but not read");
            }
            if ppe_flags & (FLAG_R | FLAG_X) == 0 {
                // todo - check R W X & U flags

                let offset_mask = (1 << (12 + i * 9)) - 1;
                let addr_offset = addr & offset_mask;
                let page = (ppe << 2) & !offset_mask;

                return Ok(page | addr_offset);
            }

            page_ptr = (ppe & !0x3FF) << 2;
        }

        Err(TranslationError::PageFault)
    }
}
impl<'a, M: Memory> Translation<'a, M> {
    pub fn len(&self) -> u64 {
        u64::MAX
    }

    pub fn load64(&self, addr: u64) -> Result<u64, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.load64(addr)?)
    }
    pub fn load32(&self, addr: u64) -> Result<u32, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.load32(addr)?)
    }
    pub fn load16(&self, addr: u64) -> Result<u16, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.load16(addr)?)
    }
    pub fn load8(&self, addr: u64) -> Result<u8, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.load8(addr)?)
    }

    pub fn store64(&mut self, addr: u64, value: u64) -> Result<(), TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.store64(addr, value)?)
    }
    pub fn store32(&mut self, addr: u64, value: u32) -> Result<(), TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.store32(addr, value)?)
    }
    pub fn store16(&mut self, addr: u64, value: u16) -> Result<(), TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.store16(addr, value)?)
    }
    pub fn store8(&mut self, addr: u64, value: u8) -> Result<(), TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.store8(addr, value)?)
    }

    pub fn amo_swap32(
        &mut self,
        addr: u64,
        value: u32,
        aq: bool,
        rl: bool,
    ) -> Result<u32, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_swap32(addr, value, aq, rl)?)
    }
    pub fn amo_and32(
        &mut self,
        addr: u64,
        value: u32,
        aq: bool,
        rl: bool,
    ) -> Result<u32, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_and32(addr, value, aq, rl)?)
    }
    pub fn amo_or32(
        &mut self,
        addr: u64,
        value: u32,
        aq: bool,
        rl: bool,
    ) -> Result<u32, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_or32(addr, value, aq, rl)?)
    }
    pub fn amo_xor32(
        &mut self,
        addr: u64,
        value: u32,
        aq: bool,
        rl: bool,
    ) -> Result<u32, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_xor32(addr, value, aq, rl)?)
    }
    pub fn amo_add32(
        &mut self,
        addr: u64,
        value: u32,
        aq: bool,
        rl: bool,
    ) -> Result<u32, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_add32(addr, value, aq, rl)?)
    }
    pub fn amo_min32(
        &mut self,
        addr: u64,
        value: u32,
        aq: bool,
        rl: bool,
    ) -> Result<u32, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_min32(addr, value, aq, rl)?)
    }
    pub fn amo_max32(
        &mut self,
        addr: u64,
        value: u32,
        aq: bool,
        rl: bool,
    ) -> Result<u32, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_max32(addr, value, aq, rl)?)
    }
    pub fn amo_minu32(
        &mut self,
        addr: u64,
        value: u32,
        aq: bool,
        rl: bool,
    ) -> Result<u32, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_minu32(addr, value, aq, rl)?)
    }
    pub fn amo_maxu32(
        &mut self,
        addr: u64,
        value: u32,
        aq: bool,
        rl: bool,
    ) -> Result<u32, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_maxu32(addr, value, aq, rl)?)
    }

    pub fn amo_swap64(
        &mut self,
        addr: u64,
        value: u64,
        aq: bool,
        rl: bool,
    ) -> Result<u64, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_swap64(addr, value, aq, rl)?)
    }
    pub fn amo_and64(
        &mut self,
        addr: u64,
        value: u64,
        aq: bool,
        rl: bool,
    ) -> Result<u64, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_and64(addr, value, aq, rl)?)
    }
    pub fn amo_or64(
        &mut self,
        addr: u64,
        value: u64,
        aq: bool,
        rl: bool,
    ) -> Result<u64, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_or64(addr, value, aq, rl)?)
    }
    pub fn amo_xor64(
        &mut self,
        addr: u64,
        value: u64,
        aq: bool,
        rl: bool,
    ) -> Result<u64, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_xor64(addr, value, aq, rl)?)
    }
    pub fn amo_add64(
        &mut self,
        addr: u64,
        value: u64,
        aq: bool,
        rl: bool,
    ) -> Result<u64, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_add64(addr, value, aq, rl)?)
    }
    pub fn amo_min64(
        &mut self,
        addr: u64,
        value: u64,
        aq: bool,
        rl: bool,
    ) -> Result<u64, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_min64(addr, value, aq, rl)?)
    }
    pub fn amo_max64(
        &mut self,
        addr: u64,
        value: u64,
        aq: bool,
        rl: bool,
    ) -> Result<u64, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_max64(addr, value, aq, rl)?)
    }
    pub fn amo_minu64(
        &mut self,
        addr: u64,
        value: u64,
        aq: bool,
        rl: bool,
    ) -> Result<u64, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_minu64(addr, value, aq, rl)?)
    }
    pub fn amo_maxu64(
        &mut self,
        addr: u64,
        value: u64,
        aq: bool,
        rl: bool,
    ) -> Result<u64, TranslationError> {
        let addr = self.translate(addr)?;
        Ok(self.mem.amo_maxu64(addr, value, aq, rl)?)
    }
}
