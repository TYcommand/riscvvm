use crate::Memory;

/// * CSRs are referenced by a 12-bit number
/// * CSRs are zero-extended when not 64-bit
/// * invalid write bits in a valid CSR are ignored
// pub trait CSR<M: Memory> {
//     /// CSRRW | read & write
//     /// Atomic swap of the csr specified. The value is set to value (write operation).
//     /// If read is `Some`, the value that was in the csr right before the value was set
//     /// is returned (read operation, only if read is Some).
//     fn write(&mut self, csr: u16, value: u64, read: Option<&mut u64>) -> Result<(), ()>;

//     /// CSRRS | read & set (bits)
//     /// Same as [`CSR::write`], with the value replaced with a bitmask.
//     /// For every bit set in this mask the bit in the CSR is set to 1.
//     /// If the mask is None, the value is not written to (no write operation).
//     fn bits_set(&mut self, csr: u16, mask: Option<u64>, read: Option<&mut u64>) -> Result<(), ()>;

//     /// CSRRC | read & clear (bits)
//     /// Same as [`CSR::bits_set`], with the mask specifying bits to be unset.
//     /// For every bit set in this mask the bit in the CSR is set to 0.
//     fn bits_clear(&mut self, csr: u16, mask: Option<u64>, read: Option<&mut u64>)
//         -> Result<(), ()>;

//     /// Called at the start of each function
//     fn on_instruction(&mut self, _: &mut crate::RiscV<M>) {}
// }
// /// Enable boxed CSR
// impl<T, D, M: Memory> CSR<M> for T
// where
//     T: std::ops::DerefMut<Target = D>,
//     D: CSR<M>,
// {
//     fn write(&mut self, a: u16, b: u64, c: Option<&mut u64>) -> Result<(), ()> {
//         (**self).write(a, b, c)
//     }
//     fn bits_set(&mut self, a: u16, b: Option<u64>, c: Option<&mut u64>) -> Result<(), ()> {
//         (**self).bits_set(a, b, c)
//     }
//     fn bits_clear(&mut self, a: u16, b: Option<u64>, c: Option<&mut u64>) -> Result<(), ()> {
//         (**self).bits_clear(a, b, c)
//     }
// }

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Privilege {
    User = 0b00,
    Supervisor = 0b01,
    Machine = 0b11,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct CsrFlags {
    pub readwrite: [bool; 2],
    pub privilege: Privilege,
}
impl CsrFlags {
    pub fn from_csr(csr: u16) -> Option<(u8, Self)> {
        Some((
            csr as u8,
            Self {
                readwrite: [csr & 0b010000000000 != 0, csr & 0b100000000000 != 0],
                privilege: match (csr >> 8) & 0b11 {
                    0b00 => Privilege::User,
                    0b01 => Privilege::Supervisor,
                    0b10 => return None,
                    0b11 => Privilege::Machine,
                    _ => unreachable!(),
                },
            },
        ))
    }
}

/// Default implementation of standard CSRs
pub struct StandardCSR {
    pub hart: u64,

    privilege: Privilege,
    cycle: u64,
    clock: std::time::Instant,
    interrupts: [bool; 4],

    mtrap: u64,
    mexception_deleg: u64,
    minstruction_deleg: u64,
    mipending: u16,
    mienable: u16,

    /// Supervisor Address Translation and Protection.\
    /// A.K.A. A pointer to the root page table.
    satp: u64,

    mie_global: bool,
    sie_global: bool,
}
impl Default for StandardCSR {
    fn default() -> Self {
        Self {
            hart: 0,
            privilege: Privilege::Machine,

            cycle: 0,
            clock: std::time::Instant::now(),
            interrupts: [false; 4],

            mie_global: false,
            sie_global: false,

            mtrap: 0,
            mexception_deleg: 0,
            minstruction_deleg: 0,
            mipending: 0,
            mienable: 0,
            satp: 0,
        }
    }
}

impl StandardCSR {
    pub fn new(hart: u64) -> Self {
        Self {
            hart,
            ..Default::default()
        }
    }

    pub fn read(&mut self, fpu: &mut crate::fpu::Fpu, csr: u8, flags: CsrFlags) -> Result<u64, ()> {
        'other: {
            return match (flags.privilege, flags.readwrite) {
                (Privilege::User, [false, false]) => match csr {
                    // Floating-Point CRSs
                    0x001 => {
                        // fflags
                        let exceptions = fpu.exceptions & 0b11111;
                        Ok(exceptions as u64)
                    }
                    0x002 => {
                        // frm
                        let rounding_mode = crate::fpu::RoundingMode::from(fpu.rounding) as u8;
                        Ok(rounding_mode as u64)
                    }
                    0x003 => {
                        // fcsr
                        let exceptions = fpu.exceptions & 0b11111;
                        let rounding_mode = crate::fpu::RoundingMode::from(fpu.rounding) as u8;

                        Ok((exceptions | (rounding_mode << 5)) as u64)
                    }
                    _ => Err(()),
                },
                (Privilege::User, [true, false]) => match csr {
                    _ => Err(()),
                },
                (Privilege::User, [false, true]) => match csr {
                    _ => Err(()),
                },
                (Privilege::User, [true, true]) => match csr {
                    // Unprivileged Counter/Timers
                    0x00..=0x1F => {
                        let index = csr as usize;
                        Ok(match index {
                            // not sure this is right?
                            0 => self.cycle,
                            // todo - millis? seconds? micros?
                            1 => self.clock.elapsed().as_secs(),
                            // same as cycle? todo - check this
                            2 => self.cycle,
                            // custom timers
                            _ => 0,
                        })
                    }
                    _ => Err(()),
                },

                (Privilege::Supervisor, [false, false]) => match csr {
                    0x01..=0x03 => {
                        // These do not exist in supervisor,
                        // but they do in hypervisor and machine
                        Err(())
                    }

                    // Supervisor Protection and Translation
                    0x80 => {
                        // satp
                        Ok(self.satp)
                    }
                    _ => break 'other,
                },
                (Privilege::Supervisor, [true, false]) => match csr {
                    // Debug/Trace Registers
                    0xA8 => {
                        // scontext
                        Err(())
                    }
                    _ => break 'other,
                },
                (Privilege::Supervisor, [false, true]) => match csr {
                    _ => break 'other,
                },
                (Privilege::Supervisor, [true, true]) => match csr {
                    _ => break 'other,
                },

                (Privilege::Machine, [false, false]) => match csr {
                    0x01 => {
                        // misa
                        //                v 64-bit
                        const XLEN: u64 = 2 << 62;
                        const EXTENSIONS: u64 =
                            // ZY XWVUTSRQ PONMLKJI HGFEDCBA
                            0b_00_00010100_00010001_10101101;

                        Ok(XLEN | EXTENSIONS)
                    }
                    0x02 => {
                        // medeleg
                        Ok(self.mexception_deleg)
                    }
                    0x03 => {
                        // mideleg
                        Ok(self.minstruction_deleg)
                    }

                    _ => break 'other,
                },
                (Privilege::Machine, [true, false]) => match csr {
                    _ => break 'other,
                },
                (Privilege::Machine, [false, true]) => match csr {
                    _ => break 'other,
                },
                (Privilege::Machine, [true, true]) => match csr {
                    // mvendorid, marchid, mimpid
                    0x11..=0x13 => Ok(0),
                    // mhartid
                    0x14 => Ok(self.hart),

                    _ => break 'other,
                },
            };
        }
        // 744 | b01 b11 x44
        match (csr, flags.readwrite) {
            // X Trap Setup
            (0x00, [false, false]) => {
                // status

                // Global S interupt enable
                let sie = self.sie_global as u64;
                // Global M interupt enable
                let mie = self.mie_global as u64;
                // S interupt enable bit prior to trap
                let spie = 0;
                let ube = 0;
                let mpie = 0;
                let spp = 0;
                let vs = 0;
                let mpp = 0;
                let fs = 0;
                let xs = 0;
                let mprv = 0;
                let sum = 0;
                let mxr = 0;
                let tvm = 0;
                let tw = 0;
                let tsr = 0;
                let uxl = 0;
                let sxl = 0;
                let sbe = 0;
                let mbe = 0;
                let sd = 0;

                Ok((sie << 1)
                    | (mie << 3)
                    | (spie << 5)
                    | (ube << 6)
                    | (mpie << 7)
                    | (spp << 8)
                    | (vs << 9)
                    | (mpp << 11)
                    | (fs << 13)
                    | (xs << 15)
                    | (mprv << 17)
                    | (sum << 18)
                    | (mxr << 19)
                    | (tvm << 20)
                    | (tw << 21)
                    | (tsr << 22)
                    | (uxl << 32)
                    | (sxl << 34)
                    | (sbe << 36)
                    | (mbe << 37)
                    | (sd << 63))
            }
            (0x02, [false, false]) => {
                // edeleg
                match flags.privilege {
                    Privilege::User => Err(()),
                    Privilege::Supervisor => Err(()),
                    Privilege::Machine => Ok(self.mexception_deleg),
                }
            }
            (0x03, [false, false]) => {
                // ideleg
                Err(())
            }
            (0x04, [false, false]) => {
                // ie
                Err(())
            }
            (0x05, [false, false]) => {
                // tvec
                match flags.privilege {
                    Privilege::User => Err(()),
                    Privilege::Supervisor => Err(()),
                    Privilege::Machine => Ok(self.mtrap),
                }
            }
            (0x06, [false, false]) => {
                // counteren
                Err(())
            }

            // X Trap Handling
            (0x40, [false, false]) => {
                // scratch
                Err(())
            }
            (0x41, [false, false]) => {
                // epc
                Err(())
            }
            (0x42, [false, false]) => {
                // cause
                Err(())
            }
            (0x43, [false, false]) => {
                // tval
                Err(())
            }
            (0x44, [false, false]) => {
                // ip
                Err(())
            }
            (0x4A, [false, false]) => {
                // tinst
                Err(())
            }
            (0x4B, [false, false]) => {
                // tval2
                Err(())
            }

            _ => Err(()),
        }
    }

    fn write(
        &mut self,
        fpu: &mut crate::fpu::Fpu,
        csr: u8,
        flags: CsrFlags,
        value: u64,
    ) -> Result<(), ()> {
        match (flags.privilege, flags.readwrite, csr) {
            (Privilege::User, [false, false], 0x001) => {
                // fflags
                fpu.exceptions = value as u8 & 0b11111;
                Ok(())
            }
            (Privilege::User, [false, false], 0x002) => {
                // frm
                let Ok(rounding) = crate::fpu::RoundingMode::try_from(value as u8 & 0b111) else {
                    unimplemented!();
                };
                let Ok(round) = crate::fpu::Round::try_from(rounding) else {
                    unimplemented!();
                };
                fpu.rounding = round;
                Ok(())
            }
            (Privilege::User, [false, false], 0x003) => {
                // fcsr
                let Ok(rounding) = crate::fpu::RoundingMode::try_from(value as u8 >> 5) else {
                    unimplemented!();
                };
                let Ok(round) = crate::fpu::Round::try_from(rounding) else {
                    unimplemented!();
                };
                fpu.rounding = round;
                fpu.exceptions = value as u8 & 0b11111;

                Ok(())
            }

            (Privilege::Supervisor, [false, false], 0x80) => {
                // satp

                let mode = value >> 60;
                let mode = if match mode {
                    0b0000 => true,

                    0b1000 => true,
                    0b1001 => true,
                    0b1010 => true,
                    0b1011 => true,

                    _ => false,
                } {
                    mode
                } else {
                    self.satp >> 60
                };
                let v = (value & ((1 << 60) - 1)) | (mode << 60);

                self.satp = v;
                Ok(())
            }

            (Privilege::Machine, [false, false], 0x05) => {
                // mtvec
                let value = if value & 0b11 >= 2 {
                    // 0b00 = direct -> All exceptions set pc to BASE
                    // 0b01 = vendored -> Asynchronous interrupts set pc to BASE+4×cause
                    value & !0b11
                } else {
                    value
                };
                self.mtrap = value;
                Ok(())
            }
            (Privilege::Machine, [false, false], 0x01) => {
                // misa is read-only
                Ok(())
            }
            (Privilege::Machine, [true, true], 0x11..=0x14) => {
                // mvendorid, marchid, mimpid, mhartid
                // are read-only
                Ok(())
            }

            _ => Err(()),
        }
    }
}
impl StandardCSR {
    pub fn on_instruction<M: Memory>(&mut self, _: &mut crate::RiscV<M>) {
        self.cycle = self.cycle.wrapping_add(1);
    }

    pub fn bits_write(
        &mut self,
        fpu: &mut crate::fpu::Fpu,
        csr: u16,
        write: u64,
        read: Option<&mut u64>,
    ) -> Result<(), ()> {
        let Some((csr, flags)) = CsrFlags::from_csr(csr) else {
            return Err(());
        };

        let Ok(value) = self.read(fpu, csr, flags) else {
            println!("Read error");
            return Err(());
        };
        if let Some(read) = read {
            *read = value;
        }
        let Ok(()) = self.write(fpu, csr, flags, write) else {
            println!("Write error");
            return Err(());
        };
        Ok(())
    }

    pub fn bits_set(
        &mut self,
        fpu: &mut crate::fpu::Fpu,

        csr: u16,
        mask: Option<u64>,
        read: Option<&mut u64>,
    ) -> Result<(), ()> {
        let Some((csr, flags)) = CsrFlags::from_csr(csr) else {
            return Err(());
        };

        let Ok(value) = self.read(fpu, csr, flags) else {
            println!("Read error");
            return Err(());
        };
        if let Some(read) = read {
            *read = value;
        }
        if let Some(mask) = mask {
            let Ok(()) = self.write(fpu, csr, flags, value | mask) else {
                println!("Write error");
                return Err(());
            };
        }
        Ok(())
    }

    pub fn bits_clear(
        &mut self,
        fpu: &mut crate::fpu::Fpu,

        csr: u16,
        mask: Option<u64>,
        read: Option<&mut u64>,
    ) -> Result<(), ()> {
        let Some((csr, flags)) = CsrFlags::from_csr(csr) else {
            return Err(());
        };

        let Ok(value) = self.read(fpu, csr, flags) else {
            println!("Read error");
            return Err(());
        };
        if let Some(read) = read {
            *read = value;
        }
        if let Some(mask) = mask {
            let Ok(()) = self.write(fpu, csr, flags, value & !mask) else {
                println!("Write error");
                return Err(());
            };
        }
        Ok(())
    }
}

impl StandardCSR {
    pub fn privilege(&self) -> Privilege {
        self.privilege
    }

    pub fn satp(&self) -> u64 {
        self.satp
    }
}
