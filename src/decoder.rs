use crate::{
    fpu,
    mapping::{Translation, TranslationError},
    FloatRegister, Memory, Register,
};
use fpu::MxFlOp;

mod i {
    use super::Register;
    #[inline]
    pub fn rd(instr: u32) -> Register {
        Register((instr >> 7) as u8 & 0b11111)
    }
    #[inline]
    pub fn funct3(instr: u32) -> u8 {
        (instr >> 12) as u8 & 0b111
    }
    #[inline]
    pub fn rs1(instr: u32) -> Register {
        Register((instr >> 15) as u8 & 0b11111)
    }
    #[inline]
    pub fn rs2(instr: u32) -> Register {
        Register((instr >> 20) as u8 & 0b11111)
    }
    #[inline]
    pub fn funct7(instr: u32) -> u8 {
        (instr >> 25) as u8 & 0b1111111
    }
    #[inline]
    pub fn imm_i(instr: u32) -> u16 {
        (instr >> 20) as u16 & 0b1111_11111111
    }
    #[inline]
    pub fn imm_s(instr: u32) -> u16 {
        ((instr >> 25) << 5) as u16 | ((instr >> 7) & 0b11111) as u16
    }
    #[inline]
    pub fn imm_u(instr: u32) -> u32 {
        instr & 0b11111111_11111111_11110000_00000000
    }
    #[inline]
    pub fn imm_j(instr: u32) -> u32 {
        (((instr >> 21) & 0b11_11111111) << 1)
            | (((instr >> 20) & 0b1) << 11)
            | (((instr >> 12) & 0b11111111) << 12)
            | (((instr >> 31) & 0b1) << 20)
    }
    #[inline]
    pub fn imm_b(instr: u32) -> u32 {
        ((instr >> 7) & 0b11110)
            | (((instr >> 25) & 0b111111) << 5)
            | (((instr >> 7) & 0b1) << 11)
            | (((instr >> 31) & 0b1) << 12)
    }

    #[inline]
    pub fn sign_extend(v: u64, first_unused_bit: u32) -> u64 {
        let d = (1 << (first_unused_bit + 1)) - 1;
        (v & d) | ((v >> (first_unused_bit - 1)).wrapping_mul(u64::MAX) << first_unused_bit)
    }
}
mod c {
    use super::Register;

    #[inline(always)]
    pub fn reg(r: u8) -> Register {
        Register((r & 0b111) + 8)
    }

    #[inline]
    pub fn rs1(instr: u16) -> Register {
        Register((instr >> 7) as u8 & 0b11111)
    }

    #[inline]
    pub fn rs2(instr: u16) -> Register {
        Register((instr >> 2) as u8 & 0b11111)
    }

    #[inline]
    pub fn rs2_t(instr: u16) -> Register {
        reg((instr >> 2) as u8)
    }

    #[inline]
    pub fn rs1_t(instr: u16) -> Register {
        reg((instr >> 7) as u8)
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Width {
    Byte,
    Half,
    Word,
    Double,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum FloatWidth {
    Single,
    Double,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum CsrOp {
    Write,
    Set,
    Clear,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Condition {
    Eq,
    Ne,
    LtS,
    LtU,
    GeS,
    GeU,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Math {
    Add,
    Sub,
    Sll,
    SLT,
    SLTU,
    XOr,
    SRL,
    SRA,
    Or,
    And,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Math32 {
    Add,
    Sub,
    Sll,
    SLT,
    SLTU,
    SRL,
    SRA,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum MathMul {
    Mul,
    MulHSS,
    MulHSU,
    MulHUU,
    Div,
    DivU,
    Rem,
    RemU,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum MathMul32 {
    Mul,
    Div,
    DivU,
    Rem,
    RemU,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum ImmMath16 {
    SRLI,
    SRAI,
    ANDI,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum RegMath16 {
    SUBW,
    ADDW,

    SUB,
    XOR,
    OR,
    AND,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Math16 {
    Imm { math: ImmMath16, imm: i16 },
    Reg { math: RegMath16, rs2: Register },
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum FloatCompare {
    Eq,
    Lt,
    Le,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum FloatSignType {
    Copy,
    Not,
    XOr,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct Atomic {
    pub aq: bool,
    pub rl: bool,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum AtomicOp {
    Swap,
    Add,
    XOr,
    And,
    Or,
    Min,
    Max,
    MinU,
    MaxU,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum InstructionM {
    OP {
        rd: Register,
        rs1: Register,
        rs2: Register,
        math: MathMul,
    },
    OP32 {
        rd: Register,
        rs1: Register,
        rs2: Register,
        math: MathMul32,
    },
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum InstructionA {
    Load {
        rd: Register,
        rs1: Register,
        a: Atomic,
        bit64: bool,
    },
    Store {
        rd: Register,
        rs1: Register,
        rs2: Register,
        a: Atomic,
        bit64: bool,
    },
    Op {
        rd: Register,
        rs1: Register,
        rs2: Register,
        a: Atomic,
        bit64: bool,
        op: AtomicOp,
    },
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum InstructionC {
    ADDI4SPN {
        rd: Register,
        imm: u16,
    },
    FLD {
        rd: FloatRegister,
        rs1: Register,
        imm: u16,
    },
    LW {
        rd: Register,
        rs1: Register,
        imm: u16,
    },
    LD {
        rd: Register,
        rs1: Register,
        imm: u16,
    },
    FSD {
        rs1: Register,
        rs2: FloatRegister,
        imm: u16,
    },
    SW {
        rs1: Register,
        rs2: Register,
        imm: u16,
    },
    SD {
        rs1: Register,
        rs2: Register,
        imm: u16,
    },

    NOP,
    ADDI {
        rd: Register,
        imm: i16,
    },
    ADDIW {
        rd: Register,
        imm: i16,
    },
    LI {
        rd: Register,
        imm: i16,
    },
    ADDI16SP {
        imm: i16,
    },
    LUI {
        rd: Register,
        imm: i32,
    },
    OP {
        math: Math16,
        rd: Register,
    },
    J {
        imm: i16,
    },
    BEQZ {
        rs1: Register,
        imm: i16,
    },
    BNEZ {
        rs1: Register,
        imm: i16,
    },

    SLLI {
        rd: Register,
        imm: u16,
    },
    FLDSP {
        rd: FloatRegister,
        imm: u16,
    },
    LWSP {
        rd: Register,
        imm: u16,
    },
    LDSP {
        rd: Register,
        imm: u16,
    },
    ADD {
        rs1: Register,
        rs2: Register,
    },
    JALR {
        rs1: Register,
    },
    MV {
        rs1: Register,
        rs2: Register,
    },
    JR {
        rs1: Register,
    },
    FSDSP {
        rs2: FloatRegister,
        imm: u16,
    },
    SWSP {
        rs2: Register,
        imm: u16,
    },
    SDSP {
        rs2: Register,
        imm: u16,
    },
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum InstructionI {
    LOAD {
        rd: Register,
        rs1: Register,
        imm: i16,
        width: Width,
        signed: bool,
    },

    ECall,
    EBreak,
    FENCE,
    OPIMM {
        rd: Register,
        rs1: Register,
        imm: i16,
        math: Math,
    },
    AUIPC {
        rd: Register,
        imm: i32,
    },
    OPIMM32 {
        rd: Register,
        rs1: Register,
        imm: i16,
        math: Math32,
    },
    STORE {
        rs1: Register,
        rs2: Register,
        imm: i16,
        width: Width,
    },
    OP {
        rd: Register,
        rs1: Register,
        rs2: Register,
        math: Math,
    },
    LUI {
        rd: Register,
        imm: i32,
    },
    OP32 {
        rd: Register,
        rs1: Register,
        rs2: Register,
        math: Math32,
    },

    BRANCH {
        rs1: Register,
        rs2: Register,
        condition: Condition,
        imm: i16,
    },
    JALR {
        rd: Register,
        rs1: Register,
        imm: i16,
    },
    JAL {
        rd: Register,
        imm: i32,
    },
    CSR {
        rd: Register,
        rs1: Register,
        csr: u16,
        op: CsrOp,
    },
    CSRIMM {
        rd: Register,
        rs1: u16,
        csr: u16,
        op: CsrOp,
    },
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum InstructionF {
    LOAD {
        rd: FloatRegister,
        rs1: Register,
        imm: i16,
        width: FloatWidth,
    },
    STORE {
        rs1: Register,
        rs2: FloatRegister,
        imm: i16,
        width: FloatWidth,
    },
    MULTIOP {
        rd: FloatRegister,
        rs1: FloatRegister,
        rs2: FloatRegister,
        rs3: FloatRegister,
        width: FloatWidth,
        rounding_mode: fpu::RoundingMode,
        op: MxFlOp,
    },
    MVTO {
        rd: Register,
        rs1: FloatRegister,
        width: FloatWidth,
    },
    MVFROM {
        rd: FloatRegister,
        rs1: Register,
        width: FloatWidth,
    },
    CMP {
        rd: Register,
        rs1: FloatRegister,
        rs2: FloatRegister,
        width: FloatWidth,
        comp: FloatCompare,
    },
    CVT {
        rd: FloatRegister,
        rs1: FloatRegister,
        rounding_mode: fpu::RoundingMode,
        from: FloatWidth,
    },
    CVTFROM {
        rd: FloatRegister,
        rs1: Register,
        bit64: bool,
        signed: bool,
        width: FloatWidth,
        rounding_mode: fpu::RoundingMode,
    },
    CVTTO {
        rd: Register,
        rs1: FloatRegister,
        bit64: bool,
        signed: bool,
        width: FloatWidth,
        rounding_mode: fpu::RoundingMode,
    },
    SGNJ {
        rd: FloatRegister,
        rs1: FloatRegister,
        rs2: FloatRegister,
        width: FloatWidth,
        sign: FloatSignType,
    },
    OP {
        rd: FloatRegister,
        rs1: FloatRegister,
        rs2: FloatRegister,
        width: FloatWidth,
        rounding_mode: fpu::RoundingMode,
        op: fpu::FlOp,
    },
    CLASS {
        rd: Register,
        rs1: FloatRegister,
        width: FloatWidth,
    },
}

impl From<InstructionI> for Instruction {
    fn from(value: InstructionI) -> Self {
        Self::I(value)
    }
}
impl From<InstructionC> for Instruction {
    fn from(value: InstructionC) -> Self {
        Self::C(value)
    }
}
impl From<InstructionF> for Instruction {
    fn from(value: InstructionF) -> Self {
        Self::F(value)
    }
}
impl From<InstructionA> for Instruction {
    fn from(value: InstructionA) -> Self {
        Self::A(value)
    }
}
impl From<InstructionM> for Instruction {
    fn from(value: InstructionM) -> Self {
        Self::M(value)
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Instruction {
    I(InstructionI),
    F(InstructionF),
    C(InstructionC),
    A(InstructionA),
    M(InstructionM),
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum InstructionError {
    Zero,
    Invalid,
    Fetch,
}
impl std::fmt::Display for InstructionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            InstructionError::Zero => "The zero instruction is invalid",
            InstructionError::Invalid => "An invalid instruction was read",
            InstructionError::Fetch => "An instruction fetch exception",
        })
    }
}
impl std::error::Error for InstructionError {}
impl From<TranslationError> for InstructionError {
    fn from(_value: TranslationError) -> Self {
        Self::Fetch
    }
}

pub fn decode<M: Memory>(
    at: u64,
    mem: &mut Translation<M>,
) -> Result<Instruction, InstructionError> {
    use InstructionA as A;
    use InstructionC as C;
    use InstructionF as F;
    use InstructionI as I;
    use InstructionM as M;

    let instr = mem.load16(at)?;

    if instr == 0 {
        return Err(InstructionError::Zero);
    }

    let is_16_bit = instr & 0b11 != 0b11;

    Ok(if is_16_bit {
        let ifunct = (instr & 0b11) | ((instr >> 11) & 0b111_00);
        // I will be rating some of the immediate formats here
        match ifunct {
            // C.ADDI4SPN
            0b000_00 => {
                let rd = c::rs2_t(instr);

                let imm = (((instr >> 5) & 0b1) << 3)
                    | (((instr >> 6) & 0b1) << 2)
                    | (((instr >> 7) & 0b1111) << 6)
                    | (((instr >> 11) & 0b11) << 4);
                if imm == 0 {
                    return Err(InstructionError::Invalid);
                }

                C::ADDI4SPN { rd, imm }
            }
            // C.FLD
            0b001_00 => {
                let rd: FloatRegister = c::rs2_t(instr).into();
                let rs1 = c::rs1_t(instr);
                // Rating: 1 / 10 - somewhat boring
                let imm = (((instr >> 5) & 0b11) << 6) | (((instr >> 10) & 0b111) << 3);

                C::FLD { rd, rs1, imm }
            }
            // C.LW
            0b010_00 => {
                let rd = c::rs2_t(instr);
                let rs1 = c::rs1_t(instr);
                // Rating: 4 / 10 - I'm seeing some origional ideas here
                let imm = (((instr >> 5) & 0b1) << 6)
                    | (((instr >> 6) & 0b1) << 2)
                    | (((instr >> 10) & 0b111) << 3);

                C::LW { rd, rs1, imm }
            }
            // C.LD
            0b011_00 => {
                let rd = c::rs2_t(instr);
                let rs1 = c::rs1_t(instr);
                // Rating: 2 / 10 - Again, boring, but consistent at least
                let imm = (((instr >> 5) & 0b11) << 6) | (((instr >> 10) & 0b111) << 3);

                C::LD { rd, rs1, imm }
            }
            0b100_00 => unimplemented!(),
            // C.FSD
            0b101_00 => {
                let rs2: FloatRegister = c::rs2_t(instr).into();
                let rs1 = c::rs1_t(instr);
                let imm = (((instr >> 5) & 0b11) << 6) | (((instr >> 10) & 0b111) << 3);

                C::FSD { rs1, rs2, imm }
            }
            // C.SW
            0b110_00 => {
                let rs2 = c::rs2_t(instr);
                let rs1 = c::rs1_t(instr);
                let imm = (((instr >> 5) & 0b1) << 6)
                    | (((instr >> 6) & 0b1) << 2)
                    | (((instr >> 10) & 0b111) << 3);

                C::SW { rs1, rs2, imm }
            }
            // C.SD
            0b111_00 => {
                let rs2 = c::rs2_t(instr);
                let rs1 = c::rs1_t(instr);
                let imm = (((instr >> 5) & 0b11) << 6) | (((instr >> 10) & 0b111) << 3);

                C::SD { rs1, rs2, imm }
            }

            // C.ADDI
            0b000_01 => {
                let rd = c::rs1(instr);

                // Rating: 0 / 10 - entirely reasonable
                let imm = (((instr >> 2) & 0b11111) << 0) | (((instr >> 12) & 0b1) << 5);
                if imm == 0 {
                    // HINT
                }

                if rd.0 == 0 {
                    // C.NOP
                    C::NOP
                } else {
                    let imm = i::sign_extend(imm as u64, 6) as i16;
                    C::ADDI { rd, imm }
                }
            }
            // C.ADDIW
            0b001_01 => {
                let rd = c::rs1(instr);

                if rd.0 == 0 {
                    return Err(InstructionError::Invalid);
                } else {
                    let imm = (((instr >> 2) & 0b11111) << 0) | (((instr >> 12) & 0b1) << 5);
                    let imm = i::sign_extend(imm as u64, 6) as i16;

                    C::ADDIW { rd, imm }
                }
            }
            // C.LI
            0b010_01 => {
                let rd = c::rs1(instr);
                if rd.0 == 0 {
                    return Err(InstructionError::Invalid);
                }
                let imm = (((instr >> 2) & 0b11111) << 0) | (((instr >> 12) & 0b1) << 5);
                let imm = i::sign_extend(imm as u64, 6) as i16;

                C::LI { rd, imm }
            }
            // C.LUI
            0b011_01 => {
                let rd = c::rs1(instr);
                if rd.0 == 0 {
                    return Err(InstructionError::Invalid);
                } else if rd.0 == 2 {
                    // C.ADDI16SP
                    // Rating: 7 / 10 - love the inconsistency in this instruction
                    let imm = (((instr >> 2) & 0b1) << 5)
                        | (((instr >> 3) & 0b11) << 7)
                        | (((instr >> 5) & 0b1) << 6)
                        | (((instr >> 6) & 0b1) << 4)
                        | (((instr >> 12) & 0b1) << 9);
                    if imm == 0 {
                        return Err(InstructionError::Invalid);
                    }
                    let imm = i::sign_extend(imm as u64, 10) as i16;

                    C::ADDI16SP { imm }
                } else {
                    // Rating: 1 / 10 - mostly reasonable
                    let imm = (((instr as u64 >> 2) & 0b1111) << 12)
                        | (((instr as u64 >> 12) & 0b1) << 17);
                    if imm == 0 {
                        return Err(InstructionError::Invalid);
                    }
                    let imm = i::sign_extend(imm, 18) as i32;

                    C::LUI { rd, imm }
                }
            }
            // C.ops
            0b100_01 => {
                let rd = c::rs1_t(instr);
                let funct = (instr >> 10) & 0b11;
                C::OP {
                    rd,
                    math: match funct {
                        // C.SRLI
                        0b00 => {
                            let imm = ((((instr >> 2) & 0b11111) << 0)
                                | (((instr >> 12) & 0b1) << 5))
                                as i16;
                            // if imm == 0 {
                            //     // HINT
                            // }
                            Math16::Imm {
                                math: ImmMath16::SRLI,
                                imm,
                            }
                        }
                        // C.SRAI
                        0b01 => {
                            let imm = ((((instr >> 2) & 0b11111) << 0)
                                | (((instr >> 12) & 0b1) << 5))
                                as i16;
                            // if imm == 0 {
                            //     // HINT
                            // }
                            Math16::Imm {
                                math: ImmMath16::SRAI,
                                imm,
                            }
                        }
                        // C.ANDI
                        0b10 => {
                            let imm =
                                (((instr >> 2) & 0b11111) << 0) | (((instr >> 12) & 0b1) << 5);

                            let imm = i::sign_extend(imm as u64, 6) as i16;
                            Math16::Imm {
                                math: ImmMath16::ANDI,
                                imm,
                            }
                        }
                        // C.ops
                        0b11 => {
                            let rs2 = c::rs2_t(instr);
                            let funct = (instr >> 5) & 0b11;
                            let v64 = (instr >> 12) & 1 == 1;

                            Math16::Reg {
                                math: if v64 {
                                    match funct {
                                        // C.SUBW
                                        0b00 => RegMath16::SUBW,
                                        // C.ADDW
                                        0b01 => RegMath16::ADDW,
                                        0b10 => return Err(InstructionError::Invalid),
                                        0b11 => return Err(InstructionError::Invalid),

                                        _ => unreachable!(),
                                    }
                                } else {
                                    match funct {
                                        // C.SUB
                                        0b00 => RegMath16::SUB,
                                        // C.XOR
                                        0b01 => RegMath16::XOR,
                                        // C.OR
                                        0b10 => RegMath16::OR,
                                        // C.AND
                                        0b11 => RegMath16::AND,

                                        _ => unreachable!(),
                                    }
                                },
                                rs2,
                            }
                        }

                        _ => unreachable!(),
                    },
                }
            }
            // C.J
            0b101_01 => {
                // Rating: 10 / 10 - prepare yourself:
                // 11 | 4 | 9:8 | 10 | 6 | 7 | 3:1 | 5
                let imm = (((instr >> 2) & 0b1) << 5)
                    | (((instr >> 3) & 0b111) << 1)
                    | (((instr >> 6) & 0b1) << 7)
                    | (((instr >> 7) & 0b1) << 6)
                    | (((instr >> 8) & 0b1) << 10)
                    | (((instr >> 9) & 0b11) << 8)
                    | (((instr >> 11) & 0b1) << 4)
                    | (((instr >> 12) & 0b1) << 11);

                let imm = i::sign_extend(imm as u64, 12) as i16;
                C::J { imm }
            }
            // C.BxxZ
            funct @ (0b110_01 | 0b111_01) => {
                let invert = funct == 0b111_01;
                let rs1 = c::rs1_t(instr);

                // Rating: 9 / 10 - This one is fun too
                let imm = (((instr >> 2) & 0b1) << 5)
                    | (((instr >> 3) & 0b11) << 1)
                    | (((instr >> 5) & 0b11) << 6)
                    | (((instr >> 10) & 0b11) << 3)
                    | (((instr >> 12) & 0b1) << 8);

                let imm = i::sign_extend(imm as u64, 9) as i16;

                if invert {
                    C::BNEZ { rs1, imm }
                } else {
                    C::BEQZ { rs1, imm }
                }
            }

            // C.SLLI
            0b000_10 => {
                let rd = c::rs1(instr);
                let imm = (((instr >> 2) & 0b11111) << 0) | (((instr >> 12) & 0b1) << 5);

                // if rd.0 == 0 {
                //     // HINT
                // }

                C::SLLI { rd, imm }
            }
            // C.FLDSP
            0b001_10 => {
                let rd: FloatRegister = c::rs1(instr).into();
                let imm = ((((instr >> 2) & 0b111) << 6)
                    | (((instr >> 5) & 0b11) << 3)
                    | (((instr >> 12) & 0b1) << 5)) as u16;

                C::FLDSP { rd, imm }
            }
            // C.LWSP
            0b010_10 => {
                let rd = c::rs1(instr);
                if rd == Register::ZERO {
                    return Err(InstructionError::Invalid);
                }

                let imm = ((((instr >> 2) & 0b11) << 6)
                    | (((instr >> 4) & 0b111) << 2)
                    | (((instr >> 12) & 0b1) << 5)) as u16;

                C::LWSP { rd, imm }
            }
            // C.LDSP
            0b011_10 => {
                let rd = c::rs1(instr);
                if rd == Register::ZERO {
                    unimplemented!();
                }

                let imm = ((((instr >> 2) & 0b111) << 6)
                    | (((instr >> 5) & 0b11) << 3)
                    | (((instr >> 12) & 0b1) << 5)) as u16;

                C::LDSP { rd, imm }
            }
            // C.JR
            0b100_10 => {
                let rs1 = c::rs1(instr);
                let rs2 = c::rs2(instr);

                if instr >> 12 & 0b1 != 0 {
                    if rs2.0 != 0 {
                        // ADD
                        // if rs1.0 == 0 {
                        //     // HINT
                        // }
                        C::ADD { rs1, rs2 }
                    } else {
                        if rs1.0 == 0 {
                            todo!("AAAH, short EBREAK!");
                        } else {
                            // C.JALR
                            C::JALR { rs1 }
                        }
                    }
                } else {
                    if rs2.0 == 0 {
                        // C.JR
                        if rs1.0 == 0 {
                            return Err(InstructionError::Invalid);
                        }
                        C::JR { rs1 }
                    } else {
                        // C.MV
                        // if rs1.0 == 0 {
                        //     // HINT
                        // }
                        C::MV { rs1, rs2 }
                    }
                }
            }
            // C.FSDSP
            0b101_10 => {
                let rs2: FloatRegister = c::rs2(instr).into();

                let imm = ((((instr >> 7) & 0b111) << 6) | (((instr >> 10) & 0b111) << 3)) as u16;

                C::FSDSP { rs2, imm }
            }
            // C.SWSP
            0b110_10 => {
                let rs2 = c::rs2(instr);
                let imm = ((((instr >> 7) & 0b11) << 6) | (((instr >> 9) & 0b1111) << 2)) as u16;

                C::SWSP { rs2, imm }
            }
            // C.SDSP
            0b111_10 => {
                let rs2 = c::rs2(instr);
                let imm = ((((instr >> 7) & 0b111) << 6) | (((instr >> 10) & 0b111) << 3)) as u16;

                C::SDSP { rs2, imm }
            }

            // 0b000_11 => unreachable!(),
            // 0b001_11 => unreachable!(),
            // 0b010_11 => unreachable!(),
            // 0b011_11 => unreachable!(),
            // 0b100_11 => unreachable!(),
            // 0b101_11 => unreachable!(),
            // 0b110_11 => unreachable!(),
            // 0b111_11 => unreachable!(),
            _ => unreachable!(),
        }
        .into()
    } else {
        let instr_b = mem.load16(at + 2)? as u32;

        let instr = instr as u32 | (instr_b << 16);

        match (instr >> 2) & 0b11111 {
            // LOAD
            0b00_000 => {
                let rd = i::rd(instr);
                let funct = i::funct3(instr);
                let rs1 = i::rs1(instr);
                let imm = i::sign_extend(i::imm_i(instr) as u64, 12) as i16;

                let signed = funct & 0b100 == 0;

                I::LOAD {
                    rd,
                    rs1,
                    imm,
                    width: match funct & 0b11 {
                        0b00 => Width::Byte,
                        0b01 => Width::Half,
                        0b10 => Width::Word,
                        0b11 => Width::Double,

                        _ => unreachable!(),
                    },
                    signed,
                }
                .into()
            }
            // LOAD-FP
            0b00_001 => {
                let rd = FloatRegister::from(i::rd(instr));
                let width = i::funct3(instr);
                let rs1 = i::rs1(instr);
                let imm = i::sign_extend(i::imm_i(instr) as u64, 12) as i16;

                let width = if width == 0b010 {
                    FloatWidth::Single
                } else if width == 0b011 {
                    FloatWidth::Double
                } else {
                    return Err(InstructionError::Invalid);
                };

                F::LOAD {
                    rd,
                    rs1,
                    imm,
                    width,
                }
                .into()
            }
            // custom-0
            0b00_010 => return Err(InstructionError::Invalid),
            // MISC-MEM
            0b00_011 => {
                // fence is a nop because we iterate over instructions
                // one by one
                I::FENCE.into()
            }
            // OP-IMM
            0b00_100 => {
                let funct = i::funct3(instr);
                let rd = i::rd(instr);
                let rs1 = i::rs1(instr);
                let imm = i::sign_extend(i::imm_i(instr) as u64, 12) as i16;

                I::OPIMM {
                    rd,
                    rs1,
                    imm,
                    math: match funct {
                        0b000 => Math::Add,
                        0b001 => Math::Sll,
                        0b010 => Math::SLT,
                        0b011 => Math::SLTU,
                        0b100 => Math::XOr,
                        0b101 => {
                            if imm & 0b0100000_00000 != 0 {
                                Math::SRA
                            } else {
                                Math::SRL
                            }
                        }
                        0b110 => Math::Or,
                        0b111 => Math::And,

                        _ => unreachable!(),
                    },
                }
                .into()
            }
            // AUIPC
            0b00_101 => {
                let rd = i::rd(instr);
                let imm = i::imm_u(instr) as i32;
                I::AUIPC { rd, imm }.into()
            }
            // OP-IMM-32
            0b00_110 => {
                let funct = i::funct3(instr);
                let rd = i::rd(instr);
                let rs1 = i::rs1(instr);
                let imm = i::sign_extend(i::imm_i(instr) as u64, 12) as i16;

                let alt = imm & 0b0100000_00000 != 0;

                I::OPIMM32 {
                    rd,
                    rs1,
                    imm,
                    math: match funct {
                        0b000 => Math32::Add,
                        0b001 => Math32::Sll,

                        0b101 if alt => Math32::SRA,
                        0b101 => Math32::SRL,

                        _ => return Err(InstructionError::Invalid),
                    },
                }
                .into()
            }
            // 48 bits
            0b00_111 => return Err(InstructionError::Invalid),

            // STORE
            0b01_000 => {
                let funct = i::funct3(instr);
                let rs1 = i::rs1(instr);
                let rs2 = i::rs2(instr);
                let imm = i::sign_extend(i::imm_s(instr) as u64, 12) as i16;

                let width = match funct & 0b011 {
                    0b00 => Width::Byte,
                    0b01 => Width::Half,
                    0b10 => Width::Word,
                    0b11 => Width::Double,
                    _ => unreachable!(),
                };

                I::STORE {
                    rs1,
                    rs2,
                    imm,
                    width,
                }
                .into()
            }
            // STORE-FP
            0b01_001 => {
                let width = i::funct3(instr);
                let rs1 = i::rs1(instr);
                let rs2 = FloatRegister::from(i::rs2(instr));
                let imm = i::sign_extend(i::imm_s(instr) as u64, 12) as i16;

                let width = if width == 0b010 {
                    FloatWidth::Single
                } else if width == 0b011 {
                    FloatWidth::Double
                } else {
                    return Err(InstructionError::Invalid);
                };

                F::STORE {
                    rs1,
                    rs2,
                    imm,
                    width,
                }
                .into()
            }
            // custom-1
            0b01_010 => return Err(InstructionError::Invalid),
            // AMO
            0b01_011 => {
                // All operations are maximally atomic anyway

                let funct = i::funct3(instr);
                let (funct5, atom) = {
                    let funct7 = i::funct7(instr);
                    (
                        funct7 >> 2,
                        Atomic {
                            aq: funct7 & 0b10 != 0,
                            rl: funct7 & 0b01 != 0,
                        },
                    )
                };
                let rd = i::rd(instr);
                let rs1 = i::rs1(instr);
                let rs2 = i::rs2(instr);

                let bit64 = match funct {
                    0b010 => false,
                    0b011 => true,

                    _ => return Err(InstructionError::Invalid),
                };

                match funct5 & 0b11111 {
                    0b00010 => {
                        // LR
                        if rs2.0 != 0 {
                            return Err(InstructionError::Invalid);
                        }
                        A::Load {
                            rd,
                            rs1,
                            a: atom,
                            bit64,
                        }
                    }
                    0b00011 => {
                        // SC
                        A::Store {
                            rd,
                            rs1,
                            rs2,
                            a: atom,
                            bit64,
                        }
                    }
                    other => A::Op {
                        rd,
                        rs1,
                        rs2,
                        a: atom,
                        bit64,
                        op: match other {
                            0b00001 => AtomicOp::Swap,
                            0b00000 => AtomicOp::Add,
                            0b00100 => AtomicOp::XOr,
                            0b01100 => AtomicOp::And,
                            0b01000 => AtomicOp::Or,
                            0b10000 => AtomicOp::Min,
                            0b10100 => AtomicOp::Max,
                            0b11000 => AtomicOp::MinU,
                            0b11100 => AtomicOp::MaxU,

                            _ => return Err(InstructionError::Invalid),
                        },
                    },
                }
                .into()
            }
            // OP
            0b01_100 => {
                let funct = i::funct3(instr);
                let funct7 = i::funct7(instr);
                let rd = i::rd(instr);
                let rs1 = i::rs1(instr);
                let rs2 = i::rs2(instr);

                if funct7 & 0b0000001 != 0 {
                    M::OP {
                        rd,
                        rs1,
                        rs2,
                        math: match funct {
                            0b000 => MathMul::Mul,
                            0b001 => MathMul::MulHSS,
                            0b010 => MathMul::MulHSU,
                            0b011 => MathMul::MulHUU,

                            0b100 => MathMul::Div,
                            0b101 => MathMul::DivU,
                            0b110 => MathMul::Rem,
                            0b111 => MathMul::RemU,

                            _ => unreachable!(),
                        },
                    }
                    .into()
                } else {
                    let alt = funct7 & 0b0100000 != 0;

                    I::OP {
                        rd,
                        rs1,
                        rs2,
                        math: match funct {
                            // ADDI
                            0b000 => {
                                if alt {
                                    Math::Sub
                                } else {
                                    Math::Add
                                }
                            }
                            // SLLI
                            0b001 => Math::Sll,
                            // SLTI
                            0b010 => Math::SLT,

                            // SLTIU
                            0b011 => Math::SLTU,
                            // XORI
                            0b100 => Math::XOr,
                            // SRxI
                            0b101 => {
                                if alt {
                                    Math::SRA
                                } else {
                                    Math::SRL
                                }
                            }
                            // ORI
                            0b110 => Math::Or,
                            // ANDI
                            0b111 => Math::And,
                            _ => unreachable!(),
                        },
                    }
                    .into()
                }
            }
            // LUI
            0b01_101 => {
                let rd = i::rd(instr);
                let imm = i::imm_u(instr) as i32;
                I::LUI { rd, imm }.into()
            }
            // OP-32
            0b01_110 => {
                let funct = i::funct3(instr);
                let funct7 = i::funct7(instr);
                let rd = i::rd(instr);
                let rs1 = i::rs1(instr);
                let rs2 = i::rs2(instr);

                let alt = funct7 & 0b0100000 != 0;

                if funct7 & 0b0000001 != 0 {
                    M::OP32 {
                        rd,
                        rs1,
                        rs2,
                        math: match funct {
                            0b000 => MathMul32::Mul,

                            0b100 => MathMul32::Div,
                            0b101 => MathMul32::DivU,
                            0b110 => MathMul32::Rem,
                            0b111 => MathMul32::RemU,

                            _ => return Err(InstructionError::Invalid),
                        },
                    }
                    .into()
                } else {
                    I::OP32 {
                        rd,
                        rs1,
                        rs2,
                        math: match funct {
                            0b000 if alt => Math32::Sub,
                            0b000 => Math32::Add,

                            0b001 => Math32::Sll,

                            0b101 if alt => Math32::SRA,
                            0b101 => Math32::SRL,

                            _ => return Err(InstructionError::Invalid),
                        },
                    }
                    .into()
                }
            }
            // 64 bits
            0b01_111 => return Err(InstructionError::Invalid),

            // MADD | MSUB | NMSUB | NMADD
            opcode @ (0b10_000 | 0b10_001 | 0b10_010 | 0b10_011) => {
                let rd = FloatRegister::from(i::rd(instr));
                let (rs3, width) = {
                    let funct = i::funct7(instr);
                    (FloatRegister(funct >> 2), funct & 0b11)
                };

                let funct3 = i::funct3(instr);
                let rs1 = FloatRegister::from(i::rs1(instr));
                let rs2 = FloatRegister::from(i::rs2(instr));

                let Ok(rounding_mode) = fpu::RoundingMode::try_from(funct3) else {
                    return Err(InstructionError::Invalid);
                };

                let op = match opcode {
                    0b10_000 => MxFlOp::MAdd,
                    0b10_001 => MxFlOp::MSub,
                    0b10_010 => MxFlOp::MNSub,
                    0b10_011 => MxFlOp::MNAdd,

                    _ => return Err(InstructionError::Invalid),
                };

                let width = if width == 0b00 {
                    FloatWidth::Single
                } else if width == 0b01 {
                    FloatWidth::Double
                } else {
                    return Err(InstructionError::Invalid);
                };

                F::MULTIOP {
                    rd,
                    rs1,
                    rs2,
                    rs3,
                    width,
                    rounding_mode,
                    op,
                }
                .into()
            }

            // OP-FP
            0b10_100 => {
                let rd = FloatRegister::from(i::rd(instr));
                let (funct, ftype) = {
                    let funct = i::funct7(instr);
                    (funct >> 2, funct & 0b11)
                };
                let funct3 = i::funct3(instr);
                let rs1 = FloatRegister::from(i::rs1(instr));
                let rs2 = FloatRegister::from(i::rs2(instr));

                'noexec: {
                    let op = match funct & 0b11111 {
                        0b00000 => fpu::FlOp::Add,
                        0b00001 => fpu::FlOp::Sub,
                        0b00010 => fpu::FlOp::Mul,
                        0b00011 => fpu::FlOp::Div,
                        0b01011 => fpu::FlOp::Sqrt,

                        // FM.X.W
                        0b11100 => {
                            if rs2.0 != 0 {
                                unimplemented!();
                            }
                            match funct3 {
                                0b000 => {
                                    if rs2.0 != 0 {
                                        return Err(InstructionError::Invalid);
                                    }

                                    let width = if ftype == 0b00 {
                                        FloatWidth::Single
                                    } else if ftype == 0b01 {
                                        FloatWidth::Double
                                    } else {
                                        return Err(InstructionError::Invalid);
                                    };

                                    if funct3 != 0b000 {
                                        return Err(InstructionError::Invalid);
                                    }

                                    break 'noexec F::MVTO {
                                        rd: rd.into(),
                                        rs1,
                                        width,
                                    };
                                }
                                0b001 => {
                                    let width = if ftype == 0b00 {
                                        FloatWidth::Single
                                    } else if ftype == 0b01 {
                                        FloatWidth::Double
                                    } else {
                                        return Err(InstructionError::Invalid);
                                    };

                                    break 'noexec F::CLASS {
                                        rd: rd.into(),
                                        rs1,
                                        width,
                                    };
                                }

                                _ => return Err(InstructionError::Invalid),
                            }
                        }
                        // FMV.W.X
                        0b11110 => {
                            if rs2.0 != 0 {
                                return Err(InstructionError::Invalid);
                            }
                            let width = if ftype == 0b00 {
                                FloatWidth::Single
                            } else if ftype == 0b01 {
                                FloatWidth::Double
                            } else {
                                return Err(InstructionError::Invalid);
                            };

                            break 'noexec F::MVFROM {
                                rd,
                                rs1: rs1.into(),
                                width,
                            };
                        }

                        // FMIN.S, FMAX.S
                        0b00101 => {
                            if funct3 == 0 {
                                fpu::FlOp::Min
                            } else if funct3 == 0b001 {
                                fpu::FlOp::Max
                            } else {
                                unimplemented!("{funct3:b}");
                            }
                        }

                        // FSGNJ
                        0b00100 => {
                            let sign = match funct3 {
                                0b000 => FloatSignType::Copy,
                                0b001 => FloatSignType::Not,
                                0b010 => FloatSignType::XOr,
                                _ => return Err(InstructionError::Invalid),
                            };

                            let width = if ftype == 0b00 {
                                FloatWidth::Single
                            } else if ftype == 0b01 {
                                FloatWidth::Double
                            } else {
                                return Err(InstructionError::Invalid);
                            };

                            break 'noexec F::SGNJ {
                                rd,
                                rs1,
                                rs2,
                                width,
                                sign,
                            };
                        }

                        // FCVT.W.S
                        0b11000 => {
                            let width = if ftype == 0b00 {
                                FloatWidth::Single
                            } else if ftype == 0b01 {
                                FloatWidth::Double
                            } else {
                                return Err(InstructionError::Invalid);
                            };

                            let (bit64, signed) = match rs2.0 {
                                // FCVT.W.D
                                0b00000 => (false, true),
                                // FCVT.WU.D
                                0b00001 => (false, false),
                                // FCVT.L.D
                                0b00010 => (true, true),
                                // FCVT.LU.D
                                0b00011 => (true, false),

                                _ => return Err(InstructionError::Invalid),
                            };

                            let Ok(rounding_mode) = fpu::RoundingMode::try_from(funct3) else {
                                return Err(InstructionError::Invalid);
                            };

                            break 'noexec F::CVTTO {
                                rd: rd.into(),
                                rs1,
                                bit64,
                                signed,
                                width,
                                rounding_mode,
                            };
                        }

                        // FCVT.S.W
                        0b11010 => {
                            let width = if ftype == 0b00 {
                                FloatWidth::Single
                            } else if ftype == 0b01 {
                                FloatWidth::Double
                            } else {
                                return Err(InstructionError::Invalid);
                            };

                            let (bit64, signed) = match rs2.0 {
                                // FCVT.D.W
                                0b00000 => (false, true),
                                // FCVT.D.WU
                                0b00001 => (false, false),
                                // FCVT.D.L
                                0b00010 => (true, true),
                                // FCVT.D.LU
                                0b00011 => (true, false),

                                _ => return Err(InstructionError::Invalid),
                            };

                            let Ok(rounding_mode) = fpu::RoundingMode::try_from(funct3) else {
                                return Err(InstructionError::Invalid);
                            };

                            break 'noexec F::CVTFROM {
                                rd,
                                rs1: rs1.into(),
                                bit64,
                                signed,
                                width,
                                rounding_mode,
                            };
                        }

                        // FCMP
                        0b10100 => {
                            let width = if ftype == 0b00 {
                                FloatWidth::Single
                            } else if ftype == 0b01 {
                                FloatWidth::Double
                            } else {
                                return Err(InstructionError::Invalid);
                            };

                            let comp = match funct3 {
                                // FLE
                                0b000 => FloatCompare::Le,
                                // FLT
                                0b001 => FloatCompare::Lt,
                                // FEQ
                                0b010 => FloatCompare::Eq,

                                _ => return Err(InstructionError::Invalid),
                            };

                            break 'noexec F::CMP {
                                rd: rd.into(),
                                rs1,
                                rs2,
                                width,
                                comp,
                            };
                        }

                        // FCVT.x.x
                        0b01000 => {
                            let Ok(rounding_mode) = fpu::RoundingMode::try_from(funct3) else {
                                return Err(InstructionError::Invalid);
                            };

                            let width = if ftype == 0b00 {
                                FloatWidth::Double
                            } else if ftype == 0b01 {
                                FloatWidth::Single
                            } else {
                                return Err(InstructionError::Invalid);
                            };

                            break 'noexec F::CVT {
                                rd,
                                rs1,
                                rounding_mode,
                                from: width,
                            };
                        }

                        _ => return Err(InstructionError::Invalid),
                    };

                    let Ok(rounding_mode) = fpu::RoundingMode::try_from(funct3) else {
                        return Err(InstructionError::Invalid);
                    };

                    let width = if ftype == 0b00 {
                        FloatWidth::Single
                    } else if ftype == 0b01 {
                        FloatWidth::Double
                    } else {
                        return Err(InstructionError::Invalid);
                    };
                    F::OP {
                        rd,
                        rs1,
                        rs2,
                        width,
                        rounding_mode,
                        op,
                    }
                }
                .into()
            }
            // reserved
            0b10_101 => return Err(InstructionError::Invalid),
            // custom-2/rv128
            0b10_110 => return Err(InstructionError::Invalid),
            // 48 bits
            0b10_111 => return Err(InstructionError::Invalid),

            // BRANCH
            0b11_000 => {
                let funct = i::funct3(instr);
                let imm = i::sign_extend(i::imm_b(instr) as u64, 13) as i16;
                let rs1 = i::rs1(instr);
                let rs2 = i::rs2(instr);

                I::BRANCH {
                    rs1,
                    rs2,
                    imm,
                    condition: match funct {
                        0b000 => Condition::Eq,
                        0b001 => Condition::Ne,
                        0b010 => return Err(InstructionError::Invalid),
                        0b011 => return Err(InstructionError::Invalid),

                        0b100 => Condition::LtS,
                        0b101 => Condition::GeS,
                        0b110 => Condition::LtU,
                        0b111 => Condition::GeU,

                        _ => unreachable!(),
                    },
                }
                .into()
            }
            // JALR
            0b11_001 => {
                let rd = i::rd(instr);
                let rs1 = i::rs1(instr);
                let funct = i::funct3(instr);
                let imm = i::sign_extend(i::imm_i(instr) as u64, 12) as i16;

                match funct {
                    0b000 => I::JALR { rd, rs1, imm }.into(),
                    _ => return Err(InstructionError::Invalid),
                }
            }
            // reserved
            0b11_010 => return Err(InstructionError::Invalid),
            // JAL
            0b11_011 => {
                let rd = i::rd(instr);
                let imm = i::sign_extend(i::imm_j(instr) as u64, 20) as i32;

                I::JAL { rd, imm }.into()
            }
            // SYSTEM
            0b11_100 => {
                if instr == 0b000000000000_00000_000_00000_1110011 {
                    return Ok(I::ECall.into());
                }
                if instr == 0b000000000001_00000_000_00000_1110011 {
                    return Ok(I::EBreak.into());
                }

                let funct = i::funct3(instr);
                let op = match funct & 0b11 {
                    0b01 => CsrOp::Write,
                    0b10 => CsrOp::Set,
                    0b11 => CsrOp::Clear,
                    _ => return Err(InstructionError::Invalid),
                };
                let imm = funct & 0b100 != 0;

                let csr = i::imm_i(instr);
                let rd = i::rd(instr);

                if imm {
                    let rs1 = i::rs1(instr).0 as u16;
                    I::CSRIMM { rd, rs1, csr, op }.into()
                } else {
                    let rs1 = i::rs1(instr);
                    I::CSR { rd, rs1, csr, op }.into()
                }
            }
            // reserved
            0b11_101 => return Err(InstructionError::Invalid),
            // custom-3/rv128
            0b11_110 => return Err(InstructionError::Invalid),
            // 80+ bits
            0b11_111 => return Err(InstructionError::Invalid),

            _ => unreachable!(),
        }
    })
}
