pub use rustc_apfloat::ieee::{Double as F64, Single as F32};
pub use rustc_apfloat::Float;
pub use rustc_apfloat::Round;

use crate::instruction::FloatRegister;

#[derive(Debug, Clone)]
pub struct Fpu {
    pub regs: [u64; 32],
    pub rounding: Round,
    pub exceptions: u8,
}
impl Default for Fpu {
    fn default() -> Self {
        Self {
            regs: Default::default(),
            rounding: Round::NearestTiesToEven,
            exceptions: 0,
        }
    }
}

impl Fpu {
    /// Gets the 32-bit floating-point register without checking NaN-boxing
    pub fn get_reg32_raw(&self, reg: FloatRegister) -> F32 {
        let v = self.regs[reg.0 as usize];
        F32::from_bits(v as u32 as u128)
    }
    pub fn get_reg32(&self, reg: FloatRegister) -> F32 {
        let v = self.regs[reg.0 as usize];
        if v & 0xffffffff00000000 != 0xffffffff00000000 {
            return F32::from_bits(0x7fc00000);
        }
        F32::from_bits(v as u32 as u128)
    }
    pub fn set_reg32(&mut self, reg: FloatRegister, value: F32) {
        self.regs[reg.0 as usize] = value.to_bits() as u32 as u64 | 0xffffffff00000000;
    }
    pub fn get_reg64(&self, reg: FloatRegister) -> F64 {
        let v = self.regs[reg.0 as usize];
        F64::from_bits(v as u64 as u128)
    }
    pub fn set_reg64(&mut self, reg: FloatRegister, value: F64) {
        self.regs[reg.0 as usize] = value.to_bits() as u64;
    }

    pub fn get_exception_invalid_operation(&self) -> bool {
        return self.exceptions & 0b10000 != 0;
    }
    pub fn set_exception_invalid_operation(&mut self, value: bool) {
        self.exceptions = (value as u8 * 0b10000) | (self.exceptions & !0b10000);
    }

    pub fn get_exception_divide_by_zero(&self) -> bool {
        return self.exceptions & 0b01000 != 0;
    }
    pub fn set_exception_divide_by_zero(&mut self, value: bool) {
        self.exceptions = (value as u8 * 0b01000) | (self.exceptions & !0b01000);
    }

    pub fn get_exception_overflow(&self) -> bool {
        return self.exceptions & 0b00100 != 0;
    }
    pub fn set_exception_overflow(&mut self, value: bool) {
        self.exceptions = (value as u8 * 0b00100) | (self.exceptions & !0b00100);
    }

    pub fn get_exception_underflow(&self) -> bool {
        return self.exceptions & 0b00010 != 0;
    }
    pub fn set_exception_underflow(&mut self, value: bool) {
        self.exceptions = (value as u8 * 0b00010) | (self.exceptions & !0b00010);
    }

    pub fn get_exception_inexact(&self) -> bool {
        return self.exceptions & 0b00001 != 0;
    }
    pub fn set_exception_inexact(&mut self, value: bool) {
        self.exceptions = (value as u8 * 0b00001) | (self.exceptions & !0b00001);
    }

    pub fn apply_exceptions(&mut self, exceptions: rustc_apfloat::Status) {
        if exceptions.contains(rustc_apfloat::Status::INVALID_OP) {
            self.set_exception_invalid_operation(true);
        }
        if exceptions.contains(rustc_apfloat::Status::DIV_BY_ZERO) {
            self.set_exception_divide_by_zero(true);
        }
        if exceptions.contains(rustc_apfloat::Status::OVERFLOW) {
            self.set_exception_overflow(true);
        }
        if exceptions.contains(rustc_apfloat::Status::UNDERFLOW) {
            self.set_exception_underflow(true);
        }
        if exceptions.contains(rustc_apfloat::Status::INEXACT) {
            self.set_exception_inexact(true);
        }
    }

    pub fn load32(&mut self, reg: FloatRegister, value: u32) {
        self.set_reg32(reg, F32::from_bits(value as u128));
    }
    pub fn load64(&mut self, reg: FloatRegister, value: u64) {
        self.set_reg64(reg, F64::from_bits(value as u128));
    }

    pub fn store32(&mut self, reg: FloatRegister) -> u32 {
        let f = self.get_reg32_raw(reg);
        f.to_bits() as u32
    }
    pub fn store64(&mut self, reg: FloatRegister) -> u64 {
        let f = self.get_reg64(reg);
        f.to_bits() as u64
    }

    #[inline]
    pub fn canon_nan32(v: F32) -> F32 {
        if v.is_nan() {
            F32::from_bits(0x7fc00000)
        } else {
            v
        }
    }
    #[inline]
    pub fn canon_nan64(v: F64) -> F64 {
        if v.is_nan() {
            // F64::from_bits(0xffffffff_7fc00000)
            F64::from_bits(0x7ff80000_00000000)
        } else {
            v
        }
    }

    pub fn op32(
        &mut self,
        op: FlOp,
        a: FloatRegister,
        b: FloatRegister,

        rd: FloatRegister,
        rounding: RoundingMode,
    ) {
        let a = self.get_reg32(a);
        let b = self.get_reg32(b);

        let round = rounding.try_into().unwrap_or(self.rounding);
        let res = match op {
            FlOp::Add => a.add_r(b, round),
            FlOp::Sub => a.sub_r(b, round),
            FlOp::Mul => a.mul_r(b, round),
            FlOp::Div => a.div_r(b, round),
            FlOp::Sqrt => {
                let res = ieee_apsqrt::sqrt_accurate(a.to_bits() as u32, round).0;
                rustc_apfloat::StatusAnd {
                    value: F32::from_bits(res.value as u128),
                    status: res.status,
                }
            }
            FlOp::Min => rustc_apfloat::StatusAnd {
                status: if a.is_signaling() || b.is_signaling() {
                    rustc_apfloat::Status::INVALID_OP
                } else {
                    rustc_apfloat::Status::OK
                },
                value: match (a.is_nan(), b.is_nan()) {
                    (true, true) => F32::NAN,
                    (true, false) => b,
                    (false, true) => a,
                    (false, false) => a.minimum(b),
                },
            },
            FlOp::Max => rustc_apfloat::StatusAnd {
                status: if a.is_signaling() || b.is_signaling() {
                    rustc_apfloat::Status::INVALID_OP
                } else {
                    rustc_apfloat::Status::OK
                },
                value: match (a.is_nan(), b.is_nan()) {
                    (true, true) => F32::NAN,
                    (true, false) => b,
                    (false, true) => a,
                    (false, false) => a.maximum(b),
                },
            },
        };

        self.apply_exceptions(res.status);
        let value = Self::canon_nan32(res.value);
        self.set_reg32(rd, value);
    }
    pub fn op64(
        &mut self,
        op: FlOp,
        a: FloatRegister,
        b: FloatRegister,

        rd: FloatRegister,
        rounding: RoundingMode,
    ) {
        let a = self.get_reg64(a);
        let b = self.get_reg64(b);

        let round = rounding.try_into().unwrap_or(self.rounding);
        let res = match op {
            FlOp::Add => a.add_r(b, round),
            FlOp::Sub => a.sub_r(b, round),
            FlOp::Mul => a.mul_r(b, round),
            FlOp::Div => a.div_r(b, round),
            FlOp::Sqrt => {
                let res = ieee_apsqrt::sqrt_accurate(a.to_bits() as u64, round).0;
                rustc_apfloat::StatusAnd {
                    value: F64::from_bits(res.value as u128),
                    status: res.status,
                }
            }
            FlOp::Min => rustc_apfloat::StatusAnd {
                status: if a.is_signaling() || b.is_signaling() {
                    rustc_apfloat::Status::INVALID_OP
                } else {
                    rustc_apfloat::Status::OK
                },
                value: match (a.is_nan(), b.is_nan()) {
                    (true, true) => F64::NAN,
                    (true, false) => b,
                    (false, true) => a,
                    (false, false) => a.minimum(b),
                },
            },
            FlOp::Max => rustc_apfloat::StatusAnd {
                status: if a.is_signaling() || b.is_signaling() {
                    rustc_apfloat::Status::INVALID_OP
                } else {
                    rustc_apfloat::Status::OK
                },
                value: match (a.is_nan(), b.is_nan()) {
                    (true, true) => F64::NAN,
                    (true, false) => b,
                    (false, true) => a,
                    (false, false) => a.maximum(b),
                },
            },
        };

        self.apply_exceptions(res.status);
        let value = Self::canon_nan64(res.value);
        self.set_reg64(rd, value);
    }

    pub fn multi_op32(
        &mut self,
        op: MxFlOp,
        a: FloatRegister,
        b: FloatRegister,
        c: FloatRegister,
        rd: FloatRegister,
        rounding: RoundingMode,
    ) {
        let a: F32 = self.get_reg32(a);
        let b: F32 = self.get_reg32(b);
        let c: F32 = self.get_reg32(c);

        let round = rounding.try_into().unwrap_or(self.rounding);

        let res = match op {
            MxFlOp::MAdd => a.mul_add_r(b, c, round),
            MxFlOp::MNAdd => {
                let rustc_apfloat::StatusAnd { status, value } = a.mul_add_r(b, c, round);
                rustc_apfloat::StatusAnd {
                    status,
                    value: -value,
                }
            }
            MxFlOp::MSub => a.mul_add_r(b, -c, round),
            MxFlOp::MNSub => {
                let rustc_apfloat::StatusAnd { status, value } = a.mul_add_r(b, -c, round);
                rustc_apfloat::StatusAnd {
                    status,
                    value: -value,
                }
            }
        };

        self.apply_exceptions(res.status);
        let value = Self::canon_nan32(res.value);
        self.set_reg32(rd, value);
    }
    pub fn multi_op64(
        &mut self,
        op: MxFlOp,
        a: FloatRegister,
        b: FloatRegister,
        c: FloatRegister,
        rd: FloatRegister,
        rounding: RoundingMode,
    ) {
        let a = self.get_reg64(a);
        let b = self.get_reg64(b);
        let c = self.get_reg64(c);

        let round = rounding.try_into().unwrap_or(self.rounding);

        let res = match op {
            MxFlOp::MAdd => a.mul_add_r(b, c, round),
            MxFlOp::MNAdd => {
                let rustc_apfloat::StatusAnd { status, value } = a.mul_add_r(b, c, round);
                rustc_apfloat::StatusAnd {
                    status,
                    value: -value,
                }
            }
            MxFlOp::MSub => a.mul_add_r(b, -c, round),
            MxFlOp::MNSub => {
                let rustc_apfloat::StatusAnd { status, value } = a.mul_add_r(b, -c, round);
                rustc_apfloat::StatusAnd {
                    status,
                    value: -value,
                }
            }
        };

        self.apply_exceptions(res.status);
        let value = Self::canon_nan64(res.value);
        self.set_reg64(rd, value);
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum FlOp {
    Add,
    Sub,
    Mul,
    Div,
    Sqrt,
    Min,
    Max,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum MxFlOp {
    MAdd,
    MNAdd,
    MSub,
    MNSub,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum RoundingMode {
    /// Round to Nearest, ties to Even
    RNE = 0b000,
    /// Round towards Zero
    RTZ = 0b001,
    /// Round Down (towards −∞)
    RDN = 0b010,
    /// Round Up (towards +∞)
    RUP = 0b011,
    /// Round to Nearest, ties to Max Magnitude
    RMM = 0b100,
    // In instruction’s rm field, selects dynamic rounding mode;
    // In Rounding Mode register, Invalid.
    DYN = 0b111,
}
impl TryFrom<u8> for RoundingMode {
    type Error = ();

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        Ok(match value & 0b111 {
            0b000 => Self::RNE,
            0b001 => Self::RTZ,
            0b010 => Self::RDN,
            0b011 => Self::RUP,

            0b100 => Self::RMM,
            0b101 => return Err(()),
            0b110 => return Err(()),
            0b111 => Self::DYN,

            _ => unreachable!(),
        })
    }
}
impl TryFrom<RoundingMode> for Round {
    type Error = ();

    fn try_from(value: RoundingMode) -> Result<Self, ()> {
        Ok(match value {
            RoundingMode::RNE => Self::NearestTiesToEven,
            RoundingMode::RTZ => Self::TowardZero,
            RoundingMode::RDN => Self::TowardNegative,
            RoundingMode::RUP => Self::TowardPositive,
            RoundingMode::RMM => Self::NearestTiesToAway,
            RoundingMode::DYN => return Err(()),
        })
    }
}
impl From<Round> for RoundingMode {
    fn from(value: Round) -> Self {
        match value {
            Round::NearestTiesToEven => Self::RNE,
            Round::TowardZero => Self::RTZ,
            Round::TowardNegative => Self::RDN,
            Round::TowardPositive => Self::RUP,
            Round::NearestTiesToAway => Self::RMM,
        }
    }
}
