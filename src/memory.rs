#[derive(Debug, Clone, PartialEq)]
pub enum MemoryLoadError {
    /// Some memory addresses may not be available
    Unavailable,
    /// Some memory models may only allow aligned access
    Unaligned,
    /// Some memory models may not allow loads
    NonLoad,
}
#[derive(Debug, Clone, PartialEq)]
pub enum MemoryStoreError {
    /// Some memory addresses may not be available
    Unavailable,
    /// Some memory models may only allow aligned access
    Unaligned,
    /// Some memory models may not allow stores
    NonStore,
}
#[derive(Debug, Clone, PartialEq)]
pub enum MemoryAtomicError {
    /// Some memory addresses may not be available
    Unavailable,
    /// Some memory models may only allow aligned access
    Unaligned,
    /// Some memory models may not allow AMOs, or specific operations.
    /// See [AmoPma].
    NonAmo,
}

impl From<MemoryLoadError> for MemoryAtomicError {
    fn from(value: MemoryLoadError) -> Self {
        match value {
            MemoryLoadError::Unavailable => MemoryAtomicError::Unavailable,
            MemoryLoadError::Unaligned => MemoryAtomicError::Unaligned,
            MemoryLoadError::NonLoad => MemoryAtomicError::NonAmo,
        }
    }
}
impl From<MemoryStoreError> for MemoryAtomicError {
    fn from(value: MemoryStoreError) -> Self {
        match value {
            MemoryStoreError::Unavailable => MemoryAtomicError::Unavailable,
            MemoryStoreError::Unaligned => MemoryAtomicError::Unaligned,
            MemoryStoreError::NonStore => MemoryAtomicError::NonAmo,
        }
    }
}

/// Atomic Memory Operation Physical Memory Attributes
///
/// Defines the supported atomic operations for this memory region
pub enum AmoPma {
    /// AMONone - None
    None,
    /// AMOSwap - amoswap
    Swap,
    /// AMOLogical - amoswap, amoand, amoor, amoxor
    Logical,
    /// AMOArithmetic - amoswap, amoand, amoor, amoxor, amoadd, amomin, amomax, amominu, amomaxu
    Arithmetic,
}

/// Trait for defining the length of a [MemoryRead] or [MemoryWrite],
/// allowing it to be mapped.\
/// This trait should only be implemented on a struct that
/// also implements either [MemoryRead] or [MemoryWrite].
///
/// Atomic operations are emulated from non-atomic read and writes.
/// Since only one thread may access memory at the time this should
/// be fine, but implementations may reimplement AMOs.
pub trait Memory {
    /// Returns the first undefined address.
    /// It is a logic error for the value returned to change.
    /// This should also be cheap.
    fn len(&self) -> u64;

    fn load64(&self, addr: u64) -> Result<u64, MemoryLoadError>;

    fn load32(&self, addr: u64) -> Result<u32, MemoryLoadError> {
        let v = self.load64(addr & !0b111)?;
        Ok((if addr & 0b100 != 0 { v >> 32 } else { v }) as u32)
    }
    fn load16(&self, addr: u64) -> Result<u16, MemoryLoadError> {
        let v = self.load32(addr & !0b11)?;
        Ok((if addr & 0b10 != 0 { v >> 16 } else { v }) as u16)
    }
    fn load8(&self, addr: u64) -> Result<u8, MemoryLoadError> {
        let v = self.load16(addr & !0b1)?;
        Ok((if addr & 0b1 != 0 { v >> 8 } else { v }) as u8)
    }

    fn store8(&mut self, addr: u64, value: u8) -> Result<(), MemoryStoreError>;

    fn store16(&mut self, addr: u64, value: u16) -> Result<(), MemoryStoreError> {
        let addr = addr & !0b1;
        let bytes = value.to_le_bytes();
        for (i, byte) in bytes.into_iter().enumerate() {
            self.store8(addr + i as u64, byte)?;
        }
        Ok(())
    }
    fn store32(&mut self, addr: u64, value: u32) -> Result<(), MemoryStoreError> {
        let addr = addr & !0b11;
        let bytes = value.to_le_bytes();
        for (i, byte) in bytes.into_iter().enumerate() {
            self.store8(addr + i as u64, byte)?;
        }
        Ok(())
    }
    fn store64(&mut self, addr: u64, value: u64) -> Result<(), MemoryStoreError> {
        let addr = addr & !0b111;
        let bytes = value.to_le_bytes();
        for (i, byte) in bytes.into_iter().enumerate() {
            self.store8(addr + i as u64, byte)?;
        }
        Ok(())
    }

    fn store_slice(&mut self, addr: u64, slice: &[u8]) -> Result<(), MemoryStoreError> {
        for (i, b) in slice.iter().enumerate() {
            self.store8(addr + i as u64, *b)?;
        }
        Ok(())
    }

    /// amoswap (AMOSwap)
    fn amo_swap32(
        &mut self,
        addr: u64,
        value: u32,
        _aq: bool,
        _rl: bool,
    ) -> Result<u32, MemoryAtomicError> {
        if addr & 0b11 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load32(addr)?;
        self.store32(addr, value)?;
        Ok(val)
    }

    /// amoand (AMOLogical)
    fn amo_and32(
        &mut self,
        addr: u64,
        value: u32,
        _aq: bool,
        _rl: bool,
    ) -> Result<u32, MemoryAtomicError> {
        if addr & 0b11 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load32(addr)?;
        self.store32(addr, value & val)?;
        Ok(val)
    }
    /// amoor (AMOLogical)
    fn amo_or32(
        &mut self,
        addr: u64,
        value: u32,
        _aq: bool,
        _rl: bool,
    ) -> Result<u32, MemoryAtomicError> {
        if addr & 0b11 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load32(addr)?;
        self.store32(addr, value | val)?;
        Ok(val)
    }
    /// amoxor (AMOLogical)
    fn amo_xor32(
        &mut self,
        addr: u64,
        value: u32,
        _aq: bool,
        _rl: bool,
    ) -> Result<u32, MemoryAtomicError> {
        if addr & 0b11 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load32(addr)?;
        self.store32(addr, value ^ val)?;
        Ok(val)
    }

    /// amoadd (AMOArithmetic)
    fn amo_add32(
        &mut self,
        addr: u64,
        value: u32,
        _aq: bool,
        _rl: bool,
    ) -> Result<u32, MemoryAtomicError> {
        if addr & 0b11 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load32(addr)?;
        self.store32(addr, val.wrapping_add(value))?;
        Ok(val)
    }
    /// amomin (AMOArithmetic)
    fn amo_min32(
        &mut self,
        addr: u64,
        value: u32,
        _aq: bool,
        _rl: bool,
    ) -> Result<u32, MemoryAtomicError> {
        if addr & 0b11 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load32(addr)?;
        self.store32(addr, i32::min(val as i32, value as i32) as u32)?;
        Ok(val)
    }
    /// amomax (AMOArithmetic)
    fn amo_max32(
        &mut self,
        addr: u64,
        value: u32,
        _aq: bool,
        _rl: bool,
    ) -> Result<u32, MemoryAtomicError> {
        if addr & 0b11 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load32(addr)?;
        self.store32(addr, i32::max(val as i32, value as i32) as u32)?;
        Ok(val)
    }
    /// amominu (AMOArithmetic)
    fn amo_minu32(
        &mut self,
        addr: u64,
        value: u32,
        _aq: bool,
        _rl: bool,
    ) -> Result<u32, MemoryAtomicError> {
        if addr & 0b11 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load32(addr)?;
        self.store32(addr, u32::min(val, value))?;
        Ok(val)
    }
    /// amomaxu (AMOArithmetic)
    fn amo_maxu32(
        &mut self,
        addr: u64,
        value: u32,
        _aq: bool,
        _rl: bool,
    ) -> Result<u32, MemoryAtomicError> {
        if addr & 0b11 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load32(addr)?;
        self.store32(addr, u32::max(val, value))?;
        Ok(val)
    }

    /// amoswap (AMOSwap)
    fn amo_swap64(
        &mut self,
        addr: u64,
        value: u64,
        _aq: bool,
        _rl: bool,
    ) -> Result<u64, MemoryAtomicError> {
        if addr & 0b111 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load64(addr)?;
        self.store64(addr, value)?;
        Ok(val)
    }

    /// amoand (AMOLogical)
    fn amo_and64(
        &mut self,
        addr: u64,
        value: u64,
        _aq: bool,
        _rl: bool,
    ) -> Result<u64, MemoryAtomicError> {
        if addr & 0b111 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load64(addr)?;
        self.store64(addr, value & val)?;
        Ok(val)
    }
    /// amoor (AMOLogical)
    fn amo_or64(
        &mut self,
        addr: u64,
        value: u64,
        _aq: bool,
        _rl: bool,
    ) -> Result<u64, MemoryAtomicError> {
        if addr & 0b111 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load64(addr)?;
        self.store64(addr, value | val)?;
        Ok(val)
    }
    /// amoxor (AMOLogical)
    fn amo_xor64(
        &mut self,
        addr: u64,
        value: u64,
        _aq: bool,
        _rl: bool,
    ) -> Result<u64, MemoryAtomicError> {
        if addr & 0b111 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load64(addr)?;
        self.store64(addr, value ^ val)?;
        Ok(val)
    }

    /// amoadd (AMOArithmetic)
    fn amo_add64(
        &mut self,
        addr: u64,
        value: u64,
        _aq: bool,
        _rl: bool,
    ) -> Result<u64, MemoryAtomicError> {
        if addr & 0b111 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load64(addr)?;
        self.store64(addr, val.wrapping_add(value))?;
        Ok(val)
    }
    /// amomin (AMOArithmetic)
    fn amo_min64(
        &mut self,
        addr: u64,
        value: u64,
        _aq: bool,
        _rl: bool,
    ) -> Result<u64, MemoryAtomicError> {
        if addr & 0b111 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load64(addr)?;
        self.store64(addr, i64::min(val as i64, value as i64) as u64)?;
        Ok(val)
    }
    /// amomax (AMOArithmetic)
    fn amo_max64(
        &mut self,
        addr: u64,
        value: u64,
        _aq: bool,
        _rl: bool,
    ) -> Result<u64, MemoryAtomicError> {
        if addr & 0b111 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load64(addr)?;
        self.store64(addr, i64::max(val as i64, value as i64) as u64)?;
        Ok(val)
    }
    /// amominu (AMOArithmetic)
    fn amo_minu64(
        &mut self,
        addr: u64,
        value: u64,
        _aq: bool,
        _rl: bool,
    ) -> Result<u64, MemoryAtomicError> {
        if addr & 0b111 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load64(addr)?;
        self.store64(addr, u64::min(val, value))?;
        Ok(val)
    }
    /// amomaxu (AMOArithmetic)
    fn amo_maxu64(
        &mut self,
        addr: u64,
        value: u64,
        _aq: bool,
        _rl: bool,
    ) -> Result<u64, MemoryAtomicError> {
        if addr & 0b111 != 0 {
            return Err(MemoryAtomicError::Unaligned);
        }
        let val = self.load64(addr)?;
        self.store64(addr, u64::max(val, value))?;
        Ok(val)
    }

    /// Offset all reads and writes by offset bytes:
    /// ```
    /// use riscvvm::memory::{LinearMemory, Memory};
    ///
    /// let mut mem = LinearMemory::new(128).offset(128);
    /// mem.store8(200, 0x56);
    ///
    /// let read = mem.load8(200);
    /// assert_eq!(read, Ok(0x56));
    /// ```
    #[must_use]
    fn offset(self, offset: u64) -> Offset<Self>
    where
        Self: Sized,
    {
        Offset {
            offset,
            memory: self,
        }
    }
    /// Makes a memory readonly
    #[must_use]
    fn readonly(self) -> ReadOnly<Self>
    where
        Self: Sized,
    {
        ReadOnly(self)
    }

    fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

/// Makes a memory readonly through thn type system. See also [DiscardWrites]
#[repr(transparent)]
pub struct ReadOnly<M: Memory>(pub M);
impl<M: Memory> Memory for ReadOnly<M> {
    fn len(&self) -> u64 {
        self.0.len()
    }

    fn load64(&self, addr: u64) -> Result<u64, MemoryLoadError> {
        self.0.load64(addr)
    }
    fn load32(&self, addr: u64) -> Result<u32, MemoryLoadError> {
        self.0.load32(addr)
    }
    fn load16(&self, addr: u64) -> Result<u16, MemoryLoadError> {
        self.0.load16(addr)
    }
    fn load8(&self, addr: u64) -> Result<u8, MemoryLoadError> {
        self.0.load8(addr)
    }

    fn store64(&mut self, _addr: u64, _value: u64) -> Result<(), MemoryStoreError> {
        Err(MemoryStoreError::NonStore)
    }
    fn store32(&mut self, _addr: u64, _value: u32) -> Result<(), MemoryStoreError> {
        Err(MemoryStoreError::NonStore)
    }
    fn store16(&mut self, _addr: u64, _value: u16) -> Result<(), MemoryStoreError> {
        Err(MemoryStoreError::NonStore)
    }
    fn store8(&mut self, _addr: u64, _value: u8) -> Result<(), MemoryStoreError> {
        Err(MemoryStoreError::NonStore)
    }

    fn store_slice(&mut self, _addr: u64, _slice: &[u8]) -> Result<(), MemoryStoreError> {
        Err(MemoryStoreError::NonStore)
    }
}

pub struct Offset<M: Memory> {
    offset: u64,
    pub memory: M,
}
impl<M: Memory> Memory for Offset<M> {
    fn len(&self) -> u64 {
        self.offset.saturating_add(self.memory.len())
    }

    fn load64(&self, addr: u64) -> Result<u64, MemoryLoadError> {
        self.memory.load64(addr.wrapping_sub(self.offset))
    }
    fn load32(&self, addr: u64) -> Result<u32, MemoryLoadError> {
        self.memory.load32(addr.wrapping_sub(self.offset))
    }
    fn load16(&self, addr: u64) -> Result<u16, MemoryLoadError> {
        self.memory.load16(addr.wrapping_sub(self.offset))
    }
    fn load8(&self, addr: u64) -> Result<u8, MemoryLoadError> {
        self.memory.load8(addr.wrapping_sub(self.offset))
    }

    fn store64(&mut self, addr: u64, value: u64) -> Result<(), MemoryStoreError> {
        self.memory.store64(addr.wrapping_sub(self.offset), value)
    }
    fn store32(&mut self, addr: u64, value: u32) -> Result<(), MemoryStoreError> {
        self.memory.store32(addr.wrapping_sub(self.offset), value)
    }
    fn store16(&mut self, addr: u64, value: u16) -> Result<(), MemoryStoreError> {
        self.memory.store16(addr.wrapping_sub(self.offset), value)
    }
    fn store8(&mut self, addr: u64, value: u8) -> Result<(), MemoryStoreError> {
        self.memory.store8(addr.wrapping_sub(self.offset), value)
    }

    fn store_slice(&mut self, addr: u64, slice: &[u8]) -> Result<(), MemoryStoreError> {
        self.memory
            .store_slice(addr.wrapping_sub(self.offset), slice)
    }
}

pub struct LinearMemory(pub Box<[u8]>);
impl LinearMemory {
    pub fn new(bytes: usize) -> Self {
        Self(vec![0; bytes].into_boxed_slice())
    }
    pub fn new_fill(with: &[u8], length: usize) -> Self {
        let mut mem = vec![0; length].into_boxed_slice();
        for i in (0..).step_by(with.len()) {
            if i + with.len() >= mem.len() {
                break;
            }
            mem[i..][..with.len()].copy_from_slice(with);
        }
        Self(mem)
    }
    pub fn new_from_slice(slice: &[u8]) -> Self {
        Self(slice.to_vec().into_boxed_slice())
    }
}
impl std::ops::Deref for LinearMemory {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl std::ops::DerefMut for LinearMemory {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl From<Box<[u8]>> for LinearMemory {
    fn from(value: Box<[u8]>) -> Self {
        Self(value)
    }
}
impl From<LinearMemory> for Box<[u8]> {
    fn from(value: LinearMemory) -> Self {
        value.0
    }
}

impl Memory for LinearMemory {
    fn len(&self) -> u64 {
        self.0.len() as u64
    }

    fn load64(&self, addr: u64) -> Result<u64, MemoryLoadError> {
        Ok(u64::from_le_bytes(
            self.0
                .get(addr as usize..)
                .ok_or(MemoryLoadError::Unavailable)?
                .get(..8)
                .ok_or(MemoryLoadError::Unavailable)?
                .try_into()
                .map_err(|_| MemoryLoadError::Unavailable)?,
        ))
    }
    fn load32(&self, addr: u64) -> Result<u32, MemoryLoadError> {
        Ok(u32::from_le_bytes(
            self.0
                .get(addr as usize..)
                .ok_or(MemoryLoadError::Unavailable)?
                .get(..4)
                .ok_or(MemoryLoadError::Unavailable)?
                .try_into()
                .map_err(|_| MemoryLoadError::Unavailable)?,
        ))
    }
    fn load16(&self, addr: u64) -> Result<u16, MemoryLoadError> {
        Ok(u16::from_le_bytes(
            self.0
                .get(addr as usize..)
                .ok_or(MemoryLoadError::Unavailable)?
                .get(..2)
                .ok_or(MemoryLoadError::Unavailable)?
                .try_into()
                .map_err(|_| MemoryLoadError::Unavailable)?,
        ))
    }
    fn load8(&self, addr: u64) -> Result<u8, MemoryLoadError> {
        self.0
            .get(addr as usize)
            .copied()
            .ok_or(MemoryLoadError::Unavailable)
    }

    fn store64(&mut self, addr: u64, value: u64) -> Result<(), MemoryStoreError> {
        self.get_mut(addr as usize..)
            .ok_or(MemoryStoreError::Unavailable)?
            .get_mut(..8)
            .ok_or(MemoryStoreError::Unavailable)?
            .copy_from_slice(&value.to_le_bytes());
        Ok(())
    }
    fn store32(&mut self, addr: u64, value: u32) -> Result<(), MemoryStoreError> {
        self.get_mut(addr as usize..)
            .ok_or(MemoryStoreError::Unavailable)?
            .get_mut(..4)
            .ok_or(MemoryStoreError::Unavailable)?
            .copy_from_slice(&value.to_le_bytes());
        Ok(())
    }
    fn store16(&mut self, addr: u64, value: u16) -> Result<(), MemoryStoreError> {
        self.get_mut(addr as usize..)
            .ok_or(MemoryStoreError::Unavailable)?
            .get_mut(..2)
            .ok_or(MemoryStoreError::Unavailable)?
            .copy_from_slice(&value.to_le_bytes());
        Ok(())
    }
    fn store8(&mut self, addr: u64, value: u8) -> Result<(), MemoryStoreError> {
        *self
            .get_mut(addr as usize)
            .ok_or(MemoryStoreError::Unavailable)? = value;
        Ok(())
    }

    fn store_slice(&mut self, addr: u64, slice: &[u8]) -> Result<(), MemoryStoreError> {
        self.get_mut(addr as usize..)
            .ok_or(MemoryStoreError::Unavailable)?
            .get_mut(..slice.len())
            .ok_or(MemoryStoreError::Unavailable)?
            .copy_from_slice(slice);
        Ok(())
    }
}

/// Experimental compiletime mapping of memories.
pub mod map_const {
    use super::*;

    // a_start + a.len() < b_start

    pub enum AB<A, B> {
        A(A),
        B(B),
    }
    /// Maps two memories together at compiletime.\
    /// May have a very slow compile time, and has an unstable interface.
    pub struct DualMap<const A_START: u64, const B_START: u64, A: Memory, B: Memory> {
        a: A,
        b: B,
    }

    impl<const A_START: u64, const B_START: u64, A: Memory, B: Memory> DualMap<A_START, B_START, A, B> {
        pub fn map(&self, addr: u64) -> Option<(AB<&A, &B>, u64)> {
            if addr < A_START {
                return None;
            }
            if addr < B_START {
                let addr = addr - A_START;
                if addr >= self.a.len() {
                    return None;
                }
                Some((AB::A(&self.a), addr))
            } else {
                let addr = addr - B_START;
                if addr >= self.b.len() {
                    return None;
                }
                Some((AB::B(&self.b), addr))
            }
        }
        pub fn map_mut(&mut self, addr: u64) -> Option<(AB<&mut A, &mut B>, u64)> {
            if addr < A_START {
                return None;
            }
            if addr < B_START {
                let addr = addr - A_START;
                if addr >= self.a.len() {
                    return None;
                }
                Some((AB::A(&mut self.a), addr))
            } else {
                let addr = addr - B_START;
                if addr >= self.b.len() {
                    return None;
                }
                Some((AB::B(&mut self.b), addr))
            }
        }
    }
    impl<const A_START: u64, const B_START: u64, A: Memory, B: Memory> Memory
        for DualMap<A_START, B_START, A, B>
    {
        fn len(&self) -> u64 {
            B_START + self.b.len()
        }

        fn load64(&self, addr: u64) -> Result<u64, MemoryLoadError> {
            let (dev, addr) = self.map(addr).ok_or(MemoryLoadError::Unavailable)?;
            match dev {
                AB::A(a) => a.load64(addr),
                AB::B(b) => b.load64(addr),
            }
        }
        fn load32(&self, addr: u64) -> Result<u32, MemoryLoadError> {
            let (dev, addr) = self.map(addr).ok_or(MemoryLoadError::Unavailable)?;
            match dev {
                AB::A(a) => a.load32(addr),
                AB::B(b) => b.load32(addr),
            }
        }
        fn load16(&self, addr: u64) -> Result<u16, MemoryLoadError> {
            let (dev, addr) = self.map(addr).ok_or(MemoryLoadError::Unavailable)?;
            match dev {
                AB::A(a) => a.load16(addr),
                AB::B(b) => b.load16(addr),
            }
        }
        fn load8(&self, addr: u64) -> Result<u8, MemoryLoadError> {
            let (dev, addr) = self.map(addr).ok_or(MemoryLoadError::Unavailable)?;
            match dev {
                AB::A(a) => a.load8(addr),
                AB::B(b) => b.load8(addr),
            }
        }

        fn store64(&mut self, addr: u64, value: u64) -> Result<(), MemoryStoreError> {
            let (dev, addr) = self.map_mut(addr).ok_or(MemoryStoreError::Unavailable)?;
            match dev {
                AB::A(a) => a.store64(addr, value),
                AB::B(b) => b.store64(addr, value),
            }
        }
        fn store32(&mut self, addr: u64, value: u32) -> Result<(), MemoryStoreError> {
            let (dev, addr) = self.map_mut(addr).ok_or(MemoryStoreError::Unavailable)?;
            match dev {
                AB::A(a) => a.store32(addr, value),
                AB::B(b) => b.store32(addr, value),
            }
        }
        fn store16(&mut self, addr: u64, value: u16) -> Result<(), MemoryStoreError> {
            let (dev, addr) = self.map_mut(addr).ok_or(MemoryStoreError::Unavailable)?;
            match dev {
                AB::A(a) => a.store16(addr, value),
                AB::B(b) => b.store16(addr, value),
            }
        }
        fn store8(&mut self, addr: u64, value: u8) -> Result<(), MemoryStoreError> {
            let (dev, addr) = self.map_mut(addr).ok_or(MemoryStoreError::Unavailable)?;
            match dev {
                AB::A(a) => a.store8(addr, value),
                AB::B(b) => b.store8(addr, value),
            }
        }
    }

    impl<const A_START: u64, const B_START: u64, A: Memory, B: Memory> From<(A, B)>
        for DualMap<A_START, B_START, A, B>
    {
        fn from(value: (A, B)) -> Self {
            let a = value.0;
            let b = value.1;
            let a_len = a.len();

            if A_START >= B_START {
                panic!("The first memory region exists after the second region");
            }
            if A_START + a_len > B_START {
                panic!("Memory regions overlap!");
            }

            Self { a, b }
        }
    }

    /// Maps two memories together at compiletime.\
    /// May have a very slow compile time, and has an unstable interface.
    ///
    /// For the version with offsets known at compiletime, see [DualMap].
    pub struct OffsetDualMap<A: Memory, B: Memory> {
        a_start: u64,
        a: A,
        b_start: u64,
        b: B,
    }
    impl<A: Memory, B: Memory> OffsetDualMap<A, B> {
        pub fn map(&self, addr: u64) -> Option<(AB<&A, &B>, u64)> {
            if addr < self.a_start {
                return None;
            }
            if addr < self.b_start {
                let addr = addr - self.a_start;
                if addr >= self.a.len() {
                    return None;
                }
                Some((AB::A(&self.a), addr))
            } else {
                let addr = addr - self.b_start;
                if addr >= self.b.len() {
                    return None;
                }
                Some((AB::B(&self.b), addr))
            }
        }
        pub fn map_mut(&mut self, addr: u64) -> Option<(AB<&mut A, &mut B>, u64)> {
            if addr < self.a_start {
                return None;
            }
            if addr < self.b_start {
                let addr = addr - self.a_start;
                if addr >= self.a.len() {
                    return None;
                }
                Some((AB::A(&mut self.a), addr))
            } else {
                let addr = addr - self.b_start;
                if addr >= self.b.len() {
                    return None;
                }
                Some((AB::B(&mut self.b), addr))
            }
        }
    }
    impl<A: Memory, B: Memory> Memory for OffsetDualMap<A, B> {
        fn len(&self) -> u64 {
            self.b_start + self.b.len()
        }

        fn load64(&self, addr: u64) -> Result<u64, MemoryLoadError> {
            let (dev, addr) = self.map(addr).ok_or(MemoryLoadError::Unavailable)?;

            match dev {
                AB::A(a) => a.load64(addr),
                AB::B(b) => b.load64(addr),
            }
        }
        fn load32(&self, addr: u64) -> Result<u32, MemoryLoadError> {
            let (dev, addr) = self.map(addr).ok_or(MemoryLoadError::Unavailable)?;

            match dev {
                AB::A(a) => a.load32(addr),
                AB::B(b) => b.load32(addr),
            }
        }
        fn load16(&self, addr: u64) -> Result<u16, MemoryLoadError> {
            let (dev, addr) = self.map(addr).ok_or(MemoryLoadError::Unavailable)?;

            match dev {
                AB::A(a) => a.load16(addr),
                AB::B(b) => b.load16(addr),
            }
        }
        fn load8(&self, addr: u64) -> Result<u8, MemoryLoadError> {
            let (dev, addr) = self.map(addr).ok_or(MemoryLoadError::Unavailable)?;

            match dev {
                AB::A(a) => a.load8(addr),
                AB::B(b) => b.load8(addr),
            }
        }

        fn store64(&mut self, addr: u64, value: u64) -> Result<(), MemoryStoreError> {
            let (dev, addr) = self.map_mut(addr).ok_or(MemoryStoreError::Unavailable)?;
            match dev {
                AB::A(a) => a.store64(addr, value),
                AB::B(b) => b.store64(addr, value),
            }
        }
        fn store32(&mut self, addr: u64, value: u32) -> Result<(), MemoryStoreError> {
            let (dev, addr) = self.map_mut(addr).ok_or(MemoryStoreError::Unavailable)?;
            match dev {
                AB::A(a) => a.store32(addr, value),
                AB::B(b) => b.store32(addr, value),
            }
        }
        fn store16(&mut self, addr: u64, value: u16) -> Result<(), MemoryStoreError> {
            let (dev, addr) = self.map_mut(addr).ok_or(MemoryStoreError::Unavailable)?;
            match dev {
                AB::A(a) => a.store16(addr, value),
                AB::B(b) => b.store16(addr, value),
            }
        }
        fn store8(&mut self, addr: u64, value: u8) -> Result<(), MemoryStoreError> {
            let (dev, addr) = self.map_mut(addr).ok_or(MemoryStoreError::Unavailable)?;
            match dev {
                AB::A(a) => a.store8(addr, value),
                AB::B(b) => b.store8(addr, value),
            }
        }
    }

    impl<A: Memory, B: Memory> From<((u64, A), (u64, B))> for OffsetDualMap<A, B> {
        fn from(value: ((u64, A), (u64, B))) -> Self {
            let a_start = value.0 .0;
            let a = value.0 .1;
            let b_start = value.1 .0;
            let b = value.1 .1;
            let a_len = a.len();

            if a_start >= b_start {
                panic!("The first memory region exists after the second region");
            }
            if a_start + a_len > b_start {
                panic!("Memory regions overlap!");
            }

            Self {
                a_start,
                a,
                b_start,
                b,
            }
        }
    }
    impl<A: Memory, B: Memory> From<(Offset<A>, Offset<B>)> for OffsetDualMap<A, B> {
        fn from(value: (Offset<A>, Offset<B>)) -> Self {
            let a_start = value.0.offset;
            let a = value.0.memory;
            let b_start = value.1.offset;
            let b = value.1.memory;
            let a_len = a.len();

            if a_start >= b_start {
                panic!("The first memory region exists after the second region");
            }
            if a_start + a_len > b_start {
                panic!("Memory regions overlap!");
            }

            Self {
                a_start,
                a,
                b_start,
                b,
            }
        }
    }
}
