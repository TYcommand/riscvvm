mod official;
use official::*;

test!(rv64ud_p_fadd: "rv64ud-p-fadd");
test!(rv64ud_p_fclass: "rv64ud-p-fclass");
test!(rv64ud_p_fcmp: "rv64ud-p-fcmp");
test!(rv64ud_p_fcvt: "rv64ud-p-fcvt");
test!(rv64ud_p_fcvt_w: "rv64ud-p-fcvt_w");
test!(rv64ud_p_fdiv: "rv64ud-p-fdiv");
test!(rv64ud_p_fmadd: "rv64ud-p-fmadd");
test!(rv64ud_p_fmin: "rv64ud-p-fmin");
test!(rv64ud_p_ldst: "rv64ud-p-ldst");
test!(rv64ud_p_move: "rv64ud-p-move");
test!(rv64ud_p_recoding: "rv64ud-p-recoding");
test!(rv64ud_p_structural: "rv64ud-p-structural");
