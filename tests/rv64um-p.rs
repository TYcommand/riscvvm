mod official;
use official::*;

test!(rv64um_p_div: "rv64um-p-div");
test!(rv64um_p_mulhu: "rv64um-p-mulhu");
test!(rv64um_p_divu: "rv64um-p-divu");
test!(rv64um_p_mulw: "rv64um-p-mulw");
test!(rv64um_p_divuw: "rv64um-p-divuw");
test!(rv64um_p_rem: "rv64um-p-rem");
test!(rv64um_p_divw: "rv64um-p-divw");
test!(rv64um_p_remu: "rv64um-p-remu");
test!(rv64um_p_mul: "rv64um-p-mul");
test!(rv64um_p_remuw: "rv64um-p-remuw");
test!(rv64um_p_mulh: "rv64um-p-mulh");
test!(rv64um_p_remw: "rv64um-p-remw");
test!(rv64um_p_mulhsu: "rv64um-p-mulhsu");
