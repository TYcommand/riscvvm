mod official;
use official::*;

test!(rv64uf_p_fadd: "rv64uf-p-fadd");
test!(rv64uf_p_fclass: "rv64uf-p-fclass");
test!(rv64uf_p_fcmp: "rv64uf-p-fcmp");
test!(rv64uf_p_fcvt: "rv64uf-p-fcvt");
test!(rv64uf_p_fcvt_w: "rv64uf-p-fcvt_w");
test!(rv64uf_p_fdiv: "rv64uf-p-fdiv");
test!(rv64uf_p_fmadd: "rv64uf-p-fmadd");
test!(rv64uf_p_fmin: "rv64uf-p-fmin");
test!(rv64uf_p_ldst: "rv64uf-p-ldst");
test!(rv64uf_p_move: "rv64uf-p-move");
test!(rv64uf_p_recoding: "rv64uf-p-recoding");
