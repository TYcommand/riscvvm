use std::collections::BTreeMap;
use std::num::NonZeroU64;

use rustc_apfloat::Float;

use crate::decoder::*;
use crate::fpu;

use crate::csr::StandardCSR as Csr;
use crate::mapping::Translation;
use crate::{Memory, Register};

pub struct Core {
    pub pc: u64,
    pub regs: [u64; 32],
    pub fpu: fpu::Fpu,
    pub csr: Csr,
}

pub struct RiscV<M: Memory> {
    pub cores: Box<[Core]>,
    pub memory: M,
    pub atomics: BTreeMap<NonZeroU64, bool>,
}
impl<M: Memory> RiscV<M> {
    pub fn new(mem: M, cores: usize, pc: u64) -> Self {
        Self {
            cores: (0..cores)
                .into_iter()
                .map(|i| Core {
                    pc,
                    regs: [0; 32],
                    fpu: fpu::Fpu::default(),
                    csr: Csr::new(i as u64),
                })
                .collect(),
            memory: mem,
            atomics: BTreeMap::new(),
        }
    }

    /// Runs the first core for one instruction
    pub fn run(&mut self) {
        let core = &mut self.cores[0];
        let mem = Translation {
            mem: &mut self.memory,
            privilege: core.csr.privilege(),
            satp: core.csr.satp(),
        };

        core.run(mem, &mut self.atomics)
    }
}

impl Core {
    #[inline]
    pub fn get_reg(&self, reg: Register) -> u64 {
        if reg != Register::ZERO {
            self.regs[reg.0 as usize & 0b11111]
        } else {
            0
        }
    }
    #[inline]
    pub fn set_reg(&mut self, reg: Register, value: u64) {
        if reg != Register::ZERO {
            self.regs[reg.0 as usize & 0b11111] = value;
        }
    }
}

pub enum Exception {
    InvalidInstruction,
    PhysicalFault,
    PageFault,
}
impl From<crate::mapping::TranslationError> for Exception {
    fn from(value: crate::mapping::TranslationError) -> Self {
        match value {
            crate::mapping::TranslationError::PhysicalFault => Self::PhysicalFault,
            crate::mapping::TranslationError::PageFault => Self::PageFault,
        }
    }
}

pub enum InstructionSuccess {
    UpdatePC4,
    UpdatePC2,
    Jump,
}

impl Core {
    fn ecall(&mut self) {}

    fn csr(&mut self, csr: u16, rd: Register, input: Option<u64>, op: CsrOp) {
        let mut v = 0;
        let write = if rd != Register::ZERO {
            Some(&mut v)
        } else {
            None
        };

        let res = match op {
            CsrOp::Write => self
                .csr
                .bits_write(&mut self.fpu, csr, input.unwrap_or(0), write),
            CsrOp::Set => self.csr.bits_set(&mut self.fpu, csr, input, write),
            CsrOp::Clear => self.csr.bits_clear(&mut self.fpu, csr, input, write),
        };

        match res {
            Ok(()) => {
                if rd != Register::ZERO {
                    self.set_reg(rd, v);
                }
            }
            Err(()) => {}
        }
    }

    fn run_i<M: Memory>(
        &mut self,
        mut mem: Translation<M>,
        instr: InstructionI,
    ) -> Result<InstructionSuccess, Exception> {
        match instr {
            InstructionI::LOAD {
                rd,
                rs1,
                imm,
                width,
                signed,
            } => {
                let rs1 = self.get_reg(rs1);
                let imm = imm as i64 as u64;

                let addr = u64::wrapping_add(rs1, imm);
                let value = match (width, signed) {
                    (Width::Byte, false) => mem.load8(addr)? as u64,
                    (Width::Byte, true) => mem.load8(addr)? as i8 as u64,

                    (Width::Half, false) => mem.load16(addr)? as u64,
                    (Width::Half, true) => mem.load16(addr)? as i16 as u64,

                    (Width::Word, false) => mem.load32(addr)? as u64,
                    (Width::Word, true) => mem.load32(addr)? as i32 as u64,

                    (Width::Double, true) => mem.load64(addr)?,

                    (Width::Double, false) => return Err(Exception::InvalidInstruction),
                };

                self.set_reg(rd, value);
            }
            InstructionI::ECall => self.ecall(),
            InstructionI::EBreak => {}
            InstructionI::FENCE => {}
            InstructionI::OPIMM { rd, rs1, imm, math } => {
                let rs1 = self.get_reg(rs1);
                let rhs = imm as u64;
                let shift = (imm & 0b111111) as u32;

                let value = match math {
                    Math::Add => u64::wrapping_add(rs1, rhs),
                    Math::Sub => u64::wrapping_sub(rs1, rhs),
                    Math::Sll => u64::wrapping_shl(rs1, shift),
                    Math::SLT => ((rs1 as i64) < (rhs as i64)) as u64,
                    Math::SLTU => (rs1 < rhs) as u64,
                    Math::XOr => rs1 ^ rhs,
                    Math::SRL => u64::wrapping_shr(rs1, shift),
                    Math::SRA => i64::wrapping_shr(rs1 as i64, shift) as u64,
                    Math::Or => rs1 | rhs,
                    Math::And => rs1 & rhs,
                };

                self.set_reg(rd, value);
            }
            InstructionI::AUIPC { rd, imm } => {
                let value = u64::wrapping_add(self.pc, imm as u64);
                self.set_reg(rd, value);
            }
            InstructionI::OPIMM32 { rd, rs1, imm, math } => {
                let rs1 = self.get_reg(rs1) as u32;
                let rhs = imm as u32;
                let shift = rhs & 0b11111;

                let value = match math {
                    Math32::Add => u32::wrapping_add(rs1, rhs),
                    Math32::Sub => u32::wrapping_sub(rs1, rhs),
                    Math32::Sll => u32::wrapping_shl(rs1, shift),
                    Math32::SLT => ((rs1 as i32) < (rhs as i32)) as u32,
                    Math32::SLTU => (rs1 < rhs) as u32,
                    Math32::SRL => u32::wrapping_shr(rs1, shift),
                    Math32::SRA => i32::wrapping_shr(rs1 as i32, shift) as u32,
                };

                self.set_reg(rd, value as i32 as u64);
            }
            InstructionI::STORE {
                rs1,
                rs2,
                imm,
                width,
            } => {
                let rs1 = self.get_reg(rs1);
                let rs2 = self.get_reg(rs2);

                let addr = u64::wrapping_add(rs1, imm as u64);
                match width {
                    Width::Byte => mem.store8(addr, rs2 as u8),
                    Width::Half => mem.store16(addr, rs2 as u16),
                    Width::Word => mem.store32(addr, rs2 as u32),
                    Width::Double => mem.store64(addr, rs2),
                }?
            }
            InstructionI::OP { rd, rs1, rs2, math } => {
                let rs1 = self.get_reg(rs1);
                let rs2 = self.get_reg(rs2);
                let shift = (rs2 & 0b111111) as u32;

                let value = match math {
                    Math::Add => u64::wrapping_add(rs1, rs2),
                    Math::Sub => u64::wrapping_sub(rs1, rs2),
                    Math::Sll => u64::wrapping_shl(rs1, shift),
                    Math::SLT => ((rs1 as i64) < (rs2 as i64)) as u64,
                    Math::SLTU => (rs1 < rs2) as u64,
                    Math::XOr => rs1 ^ rs2,
                    Math::SRL => u64::wrapping_shr(rs1, shift),
                    Math::SRA => i64::wrapping_shr(rs1 as i64, shift) as u64,
                    Math::Or => rs1 | rs2,
                    Math::And => rs1 & rs2,
                };

                self.set_reg(rd, value);
            }
            InstructionI::LUI { rd, imm } => {
                let value = imm as u64;
                self.set_reg(rd, value);
            }
            InstructionI::OP32 { rd, rs1, rs2, math } => {
                let rs1 = self.get_reg(rs1) as u32;
                let rs2 = self.get_reg(rs2) as u32;

                let value: u32 = match math {
                    Math32::Add => u32::wrapping_add(rs1, rs2),
                    Math32::Sub => u32::wrapping_sub(rs1, rs2),
                    Math32::Sll => u32::wrapping_shl(rs1, rs2),
                    Math32::SLT => ((rs1 as i32) < (rs2 as i32)) as u32,
                    Math32::SLTU => (rs1 < rs2) as u32,
                    Math32::SRL => u32::wrapping_shr(rs1, rs2),
                    Math32::SRA => i32::wrapping_shr(rs1 as i32, rs2) as u32,
                };

                self.set_reg(rd, value as i32 as u64);
            }
            InstructionI::BRANCH {
                rs1,
                rs2,
                condition,
                imm,
            } => {
                let rs1 = self.get_reg(rs1);
                let rs2 = self.get_reg(rs2);

                let is_true = match condition {
                    Condition::Eq => rs1 == rs2,
                    Condition::Ne => rs1 != rs2,
                    Condition::LtS => (rs1 as i64) < (rs2 as i64),
                    Condition::LtU => rs1 < rs2,
                    Condition::GeS => (rs1 as i64) >= (rs2 as i64),
                    Condition::GeU => rs1 >= rs2,
                };

                if is_true {
                    self.pc = u64::wrapping_add(self.pc, imm as u64);
                    return Ok(InstructionSuccess::Jump);
                }
            }
            InstructionI::JALR { rd, rs1, imm } => {
                let rs1 = self.get_reg(rs1);
                let addr = u64::wrapping_add(rs1, imm as u64);

                self.set_reg(rd, u64::wrapping_add(self.pc, 4));
                self.pc = addr;
                return Ok(InstructionSuccess::Jump);
            }
            InstructionI::JAL { rd, imm } => {
                let imm = imm as u64;
                self.set_reg(rd, u64::wrapping_add(self.pc, 4));
                self.pc = u64::wrapping_add(self.pc, imm);
                return Ok(InstructionSuccess::Jump);
            }

            InstructionI::CSR { rd, rs1, csr, op } => {
                if rs1 != Register::ZERO {
                    self.csr(csr, rd, Some(self.get_reg(rs1)), op);
                } else {
                    self.csr(csr, rd, None, op);
                }
            }
            InstructionI::CSRIMM { rd, rs1, csr, op } => self.csr(csr, rd, Some(rs1 as u64), op),
        }

        Ok(InstructionSuccess::UpdatePC4)
    }

    fn run_a<M: Memory>(
        &mut self,
        mut mem: Translation<M>,
        instr: InstructionA,
        atomics: &mut BTreeMap<NonZeroU64, bool>,
    ) -> Result<InstructionSuccess, Exception> {
        match instr {
            InstructionA::Load {
                rd,
                rs1,
                a: _,
                bit64,
            } => {
                let Ok(zaddr) = self.get_reg(rs1).try_into() else {
                    todo!("Load addr 0");
                };
                atomics.insert(zaddr, bit64);
                let addr = Into::<u64>::into(zaddr);

                self.set_reg(
                    rd,
                    if bit64 {
                        mem.load64(addr)?
                    } else {
                        mem.load32(addr)? as i32 as u64
                    },
                );
            }
            InstructionA::Store {
                rd,
                rs1,
                rs2,
                a: _,
                bit64,
            } => {
                let Ok(zaddr) = self.get_reg(rs1).try_into() else {
                    todo!("Store addr 0");
                };
                let addr = Into::<u64>::into(zaddr);
                let rs2 = self.get_reg(rs2);

                let value = if let Some(bit) = atomics.remove(&zaddr) {
                    'didnotstore: {
                        match (bit64, bit) {
                            (true, true) => mem.store32(addr, rs2 as u32),
                            (false, false) => mem.store64(addr, rs2),

                            _ => break 'didnotstore 1,
                        }?;

                        0
                    }
                } else {
                    1
                };

                self.set_reg(rd, value);
            }
            InstructionA::Op {
                rd,
                rs1,
                rs2,
                a,
                bit64,
                op,
            } => {
                let addr = self.get_reg(rs1);
                let b = self.get_reg(rs2);

                match bit64 {
                    true => {
                        let value = match op {
                            AtomicOp::Swap => mem.amo_swap64(addr, b, a.aq, a.rl),
                            AtomicOp::Add => mem.amo_add64(addr, b, a.aq, a.rl),
                            AtomicOp::XOr => mem.amo_xor64(addr, b, a.aq, a.rl),
                            AtomicOp::And => mem.amo_and64(addr, b, a.aq, a.rl),
                            AtomicOp::Or => mem.amo_or64(addr, b, a.aq, a.rl),
                            AtomicOp::Min => mem.amo_min64(addr, b, a.aq, a.rl),
                            AtomicOp::Max => mem.amo_max64(addr, b, a.aq, a.rl),
                            AtomicOp::MinU => mem.amo_minu64(addr, b, a.aq, a.rl),
                            AtomicOp::MaxU => mem.amo_maxu64(addr, b, a.aq, a.rl),
                        }?;

                        self.set_reg(rd, value);
                    }
                    false => {
                        let b = b as u32;

                        let value = match op {
                            AtomicOp::Swap => mem.amo_swap32(addr, b, a.aq, a.rl),
                            AtomicOp::Add => mem.amo_add32(addr, b, a.aq, a.rl),
                            AtomicOp::XOr => mem.amo_xor32(addr, b, a.aq, a.rl),
                            AtomicOp::And => mem.amo_and32(addr, b, a.aq, a.rl),
                            AtomicOp::Or => mem.amo_or32(addr, b, a.aq, a.rl),
                            AtomicOp::Min => mem.amo_min32(addr, b, a.aq, a.rl),
                            AtomicOp::Max => mem.amo_max32(addr, b, a.aq, a.rl),
                            AtomicOp::MinU => mem.amo_minu32(addr, b, a.aq, a.rl),
                            AtomicOp::MaxU => mem.amo_maxu32(addr, b, a.aq, a.rl),
                        }?;

                        self.set_reg(rd, value as i32 as u64);
                    }
                }
            }
        }

        Ok(InstructionSuccess::UpdatePC4)
    }

    fn run_m<M: Memory>(
        &mut self,
        mut _mem: Translation<M>,
        instr: InstructionM,
    ) -> Result<InstructionSuccess, Exception> {
        match instr {
            InstructionM::OP { rd, rs1, rs2, math } => {
                let l = self.get_reg(rs1);
                let r = self.get_reg(rs2);

                let value = match math {
                    MathMul::Mul => u64::wrapping_mul(l, r),
                    MathMul::MulHSS => {
                        (((l as i64 as i128 as u128).wrapping_mul(r as i64 as i128 as u128)) >> 64)
                            as u64
                    }
                    MathMul::MulHSU => {
                        (((l as i64 as i128 as u128).wrapping_mul(r as u128)) >> 64) as u64
                    }
                    MathMul::MulHUU => (((l as u128).wrapping_mul(r as u128)) >> 64) as u64,
                    MathMul::Div => {
                        if r != 0 {
                            i64::wrapping_div(l as i64, r as i64) as u64
                        } else {
                            u64::MAX
                        }
                    }
                    MathMul::DivU => {
                        if r != 0 {
                            u64::wrapping_div(l, r)
                        } else {
                            u64::MAX
                        }
                    }
                    MathMul::Rem => {
                        if r != 0 {
                            i64::wrapping_rem(l as i64, r as i64) as u64
                        } else {
                            l
                        }
                    }
                    MathMul::RemU => {
                        if r != 0 {
                            u64::wrapping_rem(l, r)
                        } else {
                            l
                        }
                    }
                };

                self.set_reg(rd, value);
            }
            InstructionM::OP32 { rd, rs1, rs2, math } => {
                let l = self.get_reg(rs1) as u32;
                let r = self.get_reg(rs2) as u32;

                let value = match math {
                    MathMul32::Mul => u32::wrapping_mul(l, r),
                    MathMul32::Div => {
                        if r != 0 {
                            i32::wrapping_div(l as i32, r as i32) as u32
                        } else {
                            u32::MAX
                        }
                    }
                    MathMul32::DivU => {
                        if r != 0 {
                            u32::wrapping_div(l, r)
                        } else {
                            u32::MAX
                        }
                    }
                    MathMul32::Rem => {
                        if r != 0 {
                            i32::wrapping_rem(l as i32, r as i32) as u32
                        } else {
                            l
                        }
                    }
                    MathMul32::RemU => {
                        if r != 0 {
                            u32::wrapping_rem(l, r)
                        } else {
                            l
                        }
                    }
                };

                self.set_reg(rd, value as i32 as u64);
            }
        }
        Ok(InstructionSuccess::UpdatePC4)
    }

    fn run_c<M: Memory>(
        &mut self,
        mut mem: Translation<M>,
        instr: InstructionC,
    ) -> Result<InstructionSuccess, Exception> {
        match instr {
            InstructionC::ADDI4SPN { rd, imm } => {
                let sp = self.get_reg(Register::SP);
                let value = u64::wrapping_add(sp, imm as u64);
                self.set_reg(rd, value);
            }
            InstructionC::FLD { rd, rs1, imm } => {
                let rs1 = self.get_reg(rs1);

                let addr = u64::wrapping_add(rs1, imm as u64);
                let value = mem.load64(addr)?;
                self.fpu.load64(rd, value);
            }
            InstructionC::LW { rd, rs1, imm } => {
                let rs1 = self.get_reg(rs1);

                let addr = u64::wrapping_add(rs1, imm as u64);
                let value = mem.load32(addr)? as i32 as u64;
                self.set_reg(rd, value);
            }
            InstructionC::LD { rd, rs1, imm } => {
                let rs1 = self.get_reg(rs1);

                let addr = u64::wrapping_add(rs1, imm as u64);
                let value = mem.load64(addr)?;
                self.set_reg(rd, value);
            }
            InstructionC::FSD { rs1, rs2, imm } => {
                let rs1 = self.get_reg(rs1);
                let addr = u64::wrapping_add(rs1, imm as u64);

                let value = self.fpu.store64(rs2);
                mem.store64(addr, value)?;
            }
            InstructionC::SW { rs1, rs2, imm } => {
                let rs1 = self.get_reg(rs1);
                let addr = u64::wrapping_add(rs1, imm as u64);

                let value = self.get_reg(rs2);
                mem.store32(addr, value as u32)?;
            }
            InstructionC::SD { rs1, rs2, imm } => {
                let rs1 = self.get_reg(rs1);
                let addr = u64::wrapping_add(rs1, imm as u64);

                let value = self.get_reg(rs2);
                mem.store64(addr, value)?;
            }
            InstructionC::NOP => {}
            InstructionC::ADDI { rd, imm } => {
                let rs1 = self.get_reg(rd);
                let value = u64::wrapping_add(rs1, imm as u64);
                self.set_reg(rd, value);
            }
            InstructionC::ADDIW { rd, imm } => {
                let rs1 = self.get_reg(rd) as u32;
                let value = u32::wrapping_add(rs1, imm as u32);
                self.set_reg(rd, value as i32 as u64);
            }
            InstructionC::LI { rd, imm } => {
                self.set_reg(rd, imm as u64);
            }
            InstructionC::ADDI16SP { imm } => {
                let sp = self.get_reg(Register::SP);
                let value = u64::wrapping_add(sp, imm as u64);
                self.set_reg(Register::SP, value);
            }
            InstructionC::LUI { rd, imm } => {
                self.set_reg(rd, imm as u64);
            }
            InstructionC::OP { math, rd } => {
                let rs1 = self.get_reg(rd);

                let value = match math {
                    Math16::Imm { math, imm } => match math {
                        ImmMath16::SRLI => u64::wrapping_shr(rs1, imm as u32),
                        ImmMath16::SRAI => i64::wrapping_shr(rs1 as i64, imm as u32) as u64,
                        ImmMath16::ANDI => rs1 & imm as u64,
                    },
                    Math16::Reg { math, rs2 } => {
                        let rs2 = self.get_reg(rs2);
                        match math {
                            RegMath16::SUBW => {
                                u32::wrapping_sub(rs1 as u32, rs2 as u32) as i32 as u64
                            }
                            RegMath16::ADDW => {
                                u32::wrapping_add(rs1 as u32, rs2 as u32) as i32 as u64
                            }
                            RegMath16::SUB => u64::wrapping_sub(rs1, rs2),
                            RegMath16::XOR => rs1 ^ rs2,
                            RegMath16::OR => rs1 | rs2,
                            RegMath16::AND => rs1 & rs2,
                        }
                    }
                };

                self.set_reg(rd, value);
            }
            InstructionC::J { imm } => {
                self.pc = u64::wrapping_add(self.pc, imm as u64);
                return Ok(InstructionSuccess::Jump);
            }
            InstructionC::BEQZ { rs1, imm } => {
                let rs1 = self.get_reg(rs1);
                if rs1 == 0 {
                    self.pc = u64::wrapping_add(self.pc, imm as u64);
                    return Ok(InstructionSuccess::Jump);
                }
            }
            InstructionC::BNEZ { rs1, imm } => {
                let rs1 = self.get_reg(rs1);
                if rs1 != 0 {
                    self.pc = u64::wrapping_add(self.pc, imm as u64);
                    return Ok(InstructionSuccess::Jump);
                }
            }
            InstructionC::SLLI { rd, imm } => {
                let rs1 = self.get_reg(rd);
                let value = u64::wrapping_shl(rs1, imm as u32);
                self.set_reg(rd, value);
            }
            InstructionC::FLDSP { rd, imm } => {
                let sp = self.get_reg(Register::SP);
                let addr = u64::wrapping_add(sp, imm as u64);

                let value = mem.load64(addr)?;
                self.fpu.load64(rd, value);
            }
            InstructionC::LWSP { rd, imm } => {
                let sp = self.get_reg(Register::SP);
                let addr = u64::wrapping_add(sp, imm as u64);

                let value = mem.load32(addr)? as i32 as u64;
                self.set_reg(rd, value);
            }
            InstructionC::LDSP { rd, imm } => {
                let sp = self.get_reg(Register::SP);
                let addr = u64::wrapping_add(sp, imm as u64);

                let value = mem.load64(addr)?;
                self.set_reg(rd, value);
            }
            InstructionC::ADD { rs1, rs2 } => {
                let a = self.get_reg(rs1);
                let b = self.get_reg(rs2);

                let value = u64::wrapping_add(a, b);
                self.set_reg(rs1, value);
            }
            InstructionC::JALR { rs1 } => {
                self.set_reg(Register(1), self.pc.wrapping_add(2));
                self.pc = self.get_reg(rs1);
                return Ok(InstructionSuccess::Jump);
            }
            InstructionC::MV { rs1, rs2 } => {
                let value = self.get_reg(rs2);
                self.set_reg(rs1, value);
            }
            InstructionC::JR { rs1 } => {
                self.pc = self.get_reg(rs1);
                return Ok(InstructionSuccess::Jump);
            }
            InstructionC::FSDSP { rs2, imm } => {
                let value = self.fpu.store64(rs2);

                let sp = self.get_reg(Register::SP);
                let addr = u64::wrapping_add(sp, imm as u64);
                mem.store64(addr, value)?;
            }
            InstructionC::SWSP { rs2, imm } => {
                let value = self.get_reg(rs2);

                let sp = self.get_reg(Register::SP);
                let addr = u64::wrapping_add(sp, imm as u64);
                mem.store32(addr, value as u32)?;
            }
            InstructionC::SDSP { rs2, imm } => {
                let value = self.get_reg(rs2);

                let sp = self.get_reg(Register::SP);
                let addr = u64::wrapping_add(sp, imm as u64);
                mem.store64(addr, value)?;
            }
        }

        Ok(InstructionSuccess::UpdatePC2)
    }

    fn run_f<M: Memory>(
        &mut self,
        mut mem: Translation<M>,
        instr: InstructionF,
    ) -> Result<InstructionSuccess, Exception> {
        match instr {
            InstructionF::LOAD {
                rd,
                rs1,
                imm,
                width,
            } => {
                let rs1 = self.get_reg(rs1);
                let addr = u64::wrapping_add(rs1, imm as u64);
                match width {
                    FloatWidth::Single => {
                        let value = mem.load32(addr)?;
                        self.fpu.load32(rd, value);
                    }
                    FloatWidth::Double => {
                        let value = mem.load64(addr)?;
                        self.fpu.load64(rd, value);
                    }
                }
            }
            InstructionF::STORE {
                rs1,
                rs2,
                imm,
                width,
            } => {
                let rs1 = self.get_reg(rs1);
                let addr = u64::wrapping_add(rs1, imm as u64);
                match width {
                    FloatWidth::Single => {
                        let value = self.fpu.store32(rs2);
                        mem.store32(addr, value)?;
                    }
                    FloatWidth::Double => {
                        let value = self.fpu.store64(rs2);
                        mem.store64(addr, value)?;
                    }
                }
            }
            InstructionF::MULTIOP {
                rd,
                rs1,
                rs2,
                rs3,
                width,
                rounding_mode,
                op,
            } => match width {
                FloatWidth::Single => self.fpu.multi_op32(op, rs1, rs2, rs3, rd, rounding_mode),
                FloatWidth::Double => self.fpu.multi_op64(op, rs1, rs2, rs3, rd, rounding_mode),
            },
            InstructionF::MVTO { rd, rs1, width } => {
                let value = match width {
                    FloatWidth::Single => self.fpu.store32(rs1) as i32 as u64,
                    FloatWidth::Double => self.fpu.store64(rs1),
                };
                self.set_reg(rd, value);
            }
            InstructionF::MVFROM { rd, rs1, width } => {
                let rs1 = self.get_reg(rs1);
                match width {
                    FloatWidth::Single => self.fpu.load32(rd, rs1 as u32),
                    FloatWidth::Double => self.fpu.load64(rd, rs1),
                }
            }
            InstructionF::CMP {
                rd,
                rs1,
                rs2,
                width,
                comp,
            } => {
                let (is_true, invalid) = match width {
                    FloatWidth::Single => {
                        let a = self.fpu.get_reg32(rs1);
                        let b = self.fpu.get_reg32(rs2);
                        match comp {
                            FloatCompare::Eq => (a == b, a.is_signaling() || b.is_signaling()),
                            FloatCompare::Lt => (a < b, a.is_nan() || b.is_nan()),
                            FloatCompare::Le => (a <= b, a.is_nan() || b.is_nan()),
                        }
                    }
                    FloatWidth::Double => {
                        let a = self.fpu.get_reg64(rs1);
                        let b = self.fpu.get_reg64(rs2);
                        match comp {
                            FloatCompare::Eq => (a == b, a.is_signaling() || b.is_signaling()),
                            FloatCompare::Lt => (a < b, a.is_nan() || b.is_nan()),
                            FloatCompare::Le => (a <= b, a.is_nan() || b.is_nan()),
                        }
                    }
                };

                if invalid {
                    self.fpu.set_exception_invalid_operation(true);
                }

                self.set_reg(rd, is_true as u64);
            }
            InstructionF::CVT {
                rd,
                rs1,
                rounding_mode,
                from,
            } => {
                use rustc_apfloat::FloatConvert;
                let mut inexact = false;

                let status = match from {
                    FloatWidth::Single => {
                        let rs1 = self.fpu.get_reg32(rs1);
                        let round = rounding_mode
                            .try_into()
                            .unwrap_or_else(|_| self.fpu.rounding);

                        let rustc_apfloat::StatusAnd { status, value } =
                            rs1.convert_r(round, &mut inexact);

                        self.fpu.set_reg64(rd, value);
                        status
                    }
                    FloatWidth::Double => {
                        let rs1 = self.fpu.get_reg64(rs1);
                        let round = rounding_mode
                            .try_into()
                            .unwrap_or_else(|_| self.fpu.rounding);

                        let rustc_apfloat::StatusAnd { status, value } =
                            rs1.convert_r(round, &mut inexact);

                        self.fpu.set_reg32(rd, fpu::Fpu::canon_nan32(value));
                        status
                    }
                };

                self.fpu.apply_exceptions(status);
            }
            InstructionF::CVTFROM {
                rd,
                rs1,
                bit64,
                signed,
                width,
                rounding_mode,
            } => {
                let rs1 = self.get_reg(rs1);

                let round = rounding_mode
                    .try_into()
                    .unwrap_or_else(|_| self.fpu.rounding);

                let status = match width {
                    FloatWidth::Single => {
                        let rustc_apfloat::StatusAnd { status, value } = match (bit64, signed) {
                            (true, true) => fpu::F32::from_i128_r(rs1 as i64 as i128, round),
                            (true, false) => fpu::F32::from_u128_r(rs1 as u128, round),
                            (false, true) => fpu::F32::from_i128_r(rs1 as i32 as i128, round),
                            (false, false) => fpu::F32::from_u128_r(rs1 as u32 as u128, round),
                        };

                        self.fpu.set_reg32(rd, value);
                        status
                    }
                    FloatWidth::Double => {
                        let rustc_apfloat::StatusAnd { status, value } = match (bit64, signed) {
                            (true, true) => fpu::F64::from_i128_r(rs1 as i64 as i128, round),
                            (true, false) => fpu::F64::from_u128_r(rs1 as u128, round),
                            (false, true) => fpu::F64::from_i128_r(rs1 as i32 as i128, round),
                            (false, false) => fpu::F64::from_u128_r(rs1 as u32 as u128, round),
                        };

                        self.fpu.set_reg64(rd, value);
                        status
                    }
                };

                self.fpu.apply_exceptions(status);
            }
            InstructionF::CVTTO {
                rd,
                rs1,
                bit64,
                signed,
                width,
                rounding_mode,
            } => {
                let round = rounding_mode
                    .try_into()
                    .unwrap_or_else(|_| self.fpu.rounding);
                let mut is_exact = false;

                let (status, value) = match width {
                    FloatWidth::Single => {
                        let mut v = self.fpu.get_reg32(rs1);
                        if v.is_nan() {
                            v = fpu::F32::INFINITY;
                        }

                        match signed {
                            true => {
                                let rustc_apfloat::StatusAnd { status, value } = match bit64 {
                                    true => v.to_i128_r(64, round, &mut is_exact),
                                    false => v.to_i128_r(32, round, &mut is_exact),
                                };
                                if bit64 {
                                    (status, value as u64)
                                } else {
                                    (status, value as i32 as u64)
                                }
                            }
                            false => {
                                let rustc_apfloat::StatusAnd { status, value } = match bit64 {
                                    true => v.to_u128_r(64, round, &mut is_exact),
                                    false => v.to_u128_r(32, round, &mut is_exact),
                                };
                                if bit64 {
                                    (status, value as u64)
                                } else {
                                    (status, value as i32 as u64)
                                }
                            }
                        }
                    }
                    FloatWidth::Double => {
                        let mut v = self.fpu.get_reg64(rs1);
                        if v.is_nan() {
                            v = fpu::F64::INFINITY;
                        }

                        match signed {
                            true => {
                                let rustc_apfloat::StatusAnd { status, value } = match bit64 {
                                    true => v.to_i128_r(64, round, &mut is_exact),
                                    false => v.to_i128_r(32, round, &mut is_exact),
                                };
                                if bit64 {
                                    (status, value as u64)
                                } else {
                                    (status, value as i32 as u64)
                                }
                            }
                            false => {
                                let rustc_apfloat::StatusAnd { status, value } = match bit64 {
                                    true => v.to_u128_r(64, round, &mut is_exact),
                                    false => v.to_u128_r(32, round, &mut is_exact),
                                };
                                if bit64 {
                                    (status, value as u64)
                                } else {
                                    (status, value as i32 as u64)
                                }
                            }
                        }
                    }
                };

                self.fpu.apply_exceptions(status);
                self.set_reg(rd, value);
            }
            InstructionF::SGNJ {
                rd,
                rs1,
                rs2,
                width,
                sign,
            } => {
                fn set_sign32(a: fpu::F32, negative: bool) -> fpu::F32 {
                    a.copy_sign(if negative {
                        -fpu::F32::ZERO
                    } else {
                        fpu::F32::ZERO
                    })
                }
                fn set_sign64(a: fpu::F64, negative: bool) -> fpu::F64 {
                    a.copy_sign(if negative {
                        -fpu::F64::ZERO
                    } else {
                        fpu::F64::ZERO
                    })
                }

                match sign {
                    FloatSignType::Copy => match width {
                        FloatWidth::Single => {
                            let a = self.fpu.get_reg32(rs1);
                            let b = self.fpu.get_reg32(rs2);

                            let sign = b.is_negative();
                            self.fpu.set_reg32(rd, set_sign32(a, sign));
                        }
                        FloatWidth::Double => {
                            let a = self.fpu.get_reg64(rs1);
                            let b = self.fpu.get_reg64(rs2);

                            let sign = b.is_negative();
                            self.fpu.set_reg64(rd, set_sign64(a, sign));
                        }
                    },
                    FloatSignType::Not => match width {
                        FloatWidth::Single => {
                            let a = self.fpu.get_reg32(rs1);
                            let b = self.fpu.get_reg32(rs2);

                            let sign = !b.is_negative();
                            self.fpu.set_reg32(rd, set_sign32(a, sign));
                        }
                        FloatWidth::Double => {
                            let a = self.fpu.get_reg64(rs1);
                            let b = self.fpu.get_reg64(rs2);

                            let sign = !b.is_negative();
                            self.fpu.set_reg64(rd, set_sign64(a, sign));
                        }
                    },
                    FloatSignType::XOr => match width {
                        FloatWidth::Single => {
                            let a = self.fpu.get_reg32(rs1);
                            let b = self.fpu.get_reg32(rs2);

                            let sign = b.is_negative() ^ a.is_negative();
                            self.fpu.set_reg32(rd, set_sign32(a, sign));
                        }
                        FloatWidth::Double => {
                            let a = self.fpu.get_reg64(rs1);
                            let b = self.fpu.get_reg64(rs2);

                            let sign = b.is_negative() ^ a.is_negative();
                            self.fpu.set_reg64(rd, set_sign64(a, sign));
                        }
                    },
                }
            }
            InstructionF::OP {
                rd,
                rs1,
                rs2,
                width,
                rounding_mode,
                op,
            } => match width {
                FloatWidth::Single => self.fpu.op32(op, rs1, rs2, rd, rounding_mode),
                FloatWidth::Double => self.fpu.op64(op, rs1, rs2, rd, rounding_mode),
            },
            InstructionF::CLASS { rd, rs1, width } => {
                let value = match width {
                    FloatWidth::Single => {
                        let v = self.fpu.get_reg32(rs1);
                        let positive = !v.is_negative();

                        ((v.is_neg_infinity() as u64) << 0)
                            | (((!positive && v.is_normal()) as u64) << 1)
                            | (((!positive && v.is_denormal()) as u64) << 2)
                            | ((v.is_neg_zero() as u64) << 3)
                            | ((v.is_pos_zero() as u64) << 4)
                            | (((positive && v.is_denormal()) as u64) << 5)
                            | (((positive && v.is_normal()) as u64) << 6)
                            | ((v.is_pos_infinity() as u64) << 7)
                            | ((v.is_signaling() as u64) << 8)
                            | (((v.is_nan() && !v.is_signaling()) as u64) << 9)
                    }
                    FloatWidth::Double => {
                        let v = self.fpu.get_reg64(rs1);
                        let positive = !v.is_negative();

                        ((v.is_neg_infinity() as u64) << 0)
                            | (((!positive && v.is_normal()) as u64) << 1)
                            | (((!positive && v.is_denormal()) as u64) << 2)
                            | ((v.is_neg_zero() as u64) << 3)
                            | ((v.is_pos_zero() as u64) << 4)
                            | (((positive && v.is_denormal()) as u64) << 5)
                            | (((positive && v.is_normal()) as u64) << 6)
                            | ((v.is_pos_infinity() as u64) << 7)
                            | ((v.is_signaling() as u64) << 8)
                            | (((v.is_nan() && !v.is_signaling()) as u64) << 9)
                    }
                };
                self.set_reg(rd, value);
            }
        }
        Ok(InstructionSuccess::UpdatePC4)
    }

    fn on_exception(&mut self, _exception: Exception) {
        println!("EXCEPTION!");
    }

    pub fn exec<M: Memory>(
        &mut self,
        mem: Translation<M>,
        atomics: &mut BTreeMap<NonZeroU64, bool>,
        instr: Instruction,
    ) {
        let ret = match instr {
            Instruction::I(i) => self.run_i(mem, i),
            Instruction::F(f) => self.run_f(mem, f),
            Instruction::C(c) => self.run_c(mem, c),
            Instruction::A(a) => self.run_a(mem, a, atomics),
            Instruction::M(m) => self.run_m(mem, m),
        };

        match ret {
            Ok(InstructionSuccess::UpdatePC4) => {
                self.pc = self.pc.wrapping_add(4);
            }
            Ok(InstructionSuccess::UpdatePC2) => {
                self.pc = self.pc.wrapping_add(2);
            }
            Ok(InstructionSuccess::Jump) => {}

            Err(exception) => {
                self.on_exception(exception);
                self.pc = self.pc.wrapping_add(4);
            }
        }
    }

    /// Runs for one instruction
    pub fn run<M: Memory>(
        &mut self,
        mut mem: Translation<M>,
        atomics: &mut BTreeMap<NonZeroU64, bool>,
    ) {
        let instr = match decode(self.pc, &mut mem) {
            Ok(instr) => instr,
            Err(InstructionError::Zero | InstructionError::Invalid | InstructionError::Fetch) => {
                self.on_exception(Exception::InvalidInstruction);
                self.pc = self.pc.wrapping_add(4);
                return;
            }
        };

        self.exec(mem, atomics, instr)
    }
}
