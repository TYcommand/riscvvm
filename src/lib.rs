// Reason: grouping on flags is useful
#![allow(clippy::unusual_byte_groupings)]
// Reason: useful for contextualizing results
#![allow(clippy::identity_op)]

pub mod csr;
pub mod decoder;
pub mod emulator;
pub mod fpu;
pub mod instruction;
pub mod mapping;
pub mod memory;

pub use memory::Memory;

pub use fpu::Fpu;

pub use instruction::{FloatRegister, Register};

pub use emulator::{Core, RiscV};
