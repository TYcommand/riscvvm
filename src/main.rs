use memory::Memory;
use riscvvm::{mapping::Translation, *};

fn test_file(bytes: &[u8], visual: bool) -> Result<bool, elf::ParseError> {
    let file = elf::ElfBytes::<elf::endian::LittleEndian>::minimal_parse(&bytes)?;

    let text_header = file.section_header_by_name(".text.init").unwrap().unwrap();
    let text = file.section_data(&text_header).unwrap().0;
    let text_addr = text_header.sh_addr;
    let mem_text = memory::LinearMemory::new_from_slice(text)
        // .discard_writes()
        .offset(text_addr);

    let mem_data = {
        if let Some(data_header) = file.section_header_by_name(".data").unwrap() {
            let data_addr = data_header.sh_addr;
            let data = file.section_data(&data_header).unwrap().0;
            memory::LinearMemory::new_from_slice(data).offset(data_addr)
        } else {
            memory::LinearMemory::new(4096 * 4).offset(text_addr + text.len() as u64)
        }
    };

    let mut mem: memory::map_const::OffsetDualMap<_, _> = (mem_text, mem_data).into();

    let _ = mem.store_slice(text_addr, text);

    let mut vm = RiscV::new(mem, 1, text_addr);
    for core in vm.cores.iter_mut() {
        for reg in core.regs.iter_mut().skip(1) {
            *reg = 0x5454545454545454;
        }
    }

    // 011 10100

    if visual {
        let mut target_pc = None;
        loop {
            let mut mem = Translation {
                mem: &mut vm.memory,
                privilege: vm.cores[0].csr.privilege(),
                satp: vm.cores[0].csr.satp(),
            };
            let decode = decoder::decode(vm.cores[0].pc, &mut mem);
            print!("{:?}\n", decode);
            if let Ok(riscvvm::decoder::Instruction::I(riscvvm::decoder::InstructionI::ECall)) =
                decode
            {
                return Ok(vm.cores[0].get_reg(Register::A0) == 0);
            }

            print!("  PC: {:0>16x}\n", vm.cores[0].pc);

            vm.run();

            for (i, (reg, fp)) in vm.cores[0]
                .regs
                .iter()
                .zip(vm.cores[0].fpu.regs.iter())
                .enumerate()
            {
                print!(
                    "{0:>4}: {1:0>16x} | {1:0>64b} | {2:0>16x}\n",
                    Register(i as u8).name(),
                    reg,
                    fp,
                    // if fp & 0xffffffff00000000 == 0xffffffff00000000 {
                    //     f32::from_bits(*fp as u32) as f64
                    // } else {
                    //     f64::from_bits(*fp)
                    // },
                    // if fp & 0xffffffff00000000 == 0xffffffff00000000 {
                    //     'f'
                    // } else {
                    //     'd'
                    // }
                );
            }
            println!();

            if let Some(target) = target_pc {
                if target == vm.cores[0].pc {
                    target_pc = None;
                }
            } else {
                let mut read = String::new();
                std::io::stdin().read_line(&mut read).ok();
                if read.len() > 0 {
                    if let Ok(value) = u64::from_str_radix(read.trim(), 16) {
                        target_pc = Some(value + text_addr);
                    }
                }
            }
        }
    } else {
        loop {
            let mut mem = Translation {
                mem: &mut vm.memory,
                privilege: vm.cores[0].csr.privilege(),
                satp: vm.cores[0].csr.satp(),
            };
            let decode = decoder::decode(vm.cores[0].pc, &mut mem);
            if let Ok(riscvvm::decoder::Instruction::I(riscvvm::decoder::InstructionI::ECall)) =
                decode
            {
                return Ok(vm.cores[0].get_reg(Register::A0) == 0);
            }
            vm.run();
        }
    }
}

fn main() {
    let args = std::env::args_os().skip(1).collect::<Vec<_>>();

    if args.len() < 2 {
        let path = std::env::args()
            .skip(1)
            .next()
            .unwrap_or(String::from("../riscv-tests/isa/rv64ui-p-addi"));
        let file_bytes = std::fs::read(path).unwrap();
        match test_file(&file_bytes, true) {
            Ok(true) => {
                println!("Success");
                std::process::exit(0);
            }
            Ok(false) => {
                println!("Failure");
                std::process::exit(1);
            }
            Err(_) => {
                println!("Not an ELF file");
                std::process::exit(2);
            }
        }
    } else {
        let mut failures = 0;
        let mut count = 0;
        for path in args {
            let file_bytes = std::fs::read(&path).unwrap();
            match test_file(&file_bytes, false) {
                Ok(true) => {
                    println!("SUCCESS : {}", path.to_string_lossy());
                }
                Ok(false) => {
                    failures += 1;
                    println!("FAILURE : {}", path.to_string_lossy());
                }
                Err(_) => {
                    continue;
                }
            }
            count += 1;
        }
        if failures == 0 {
            println!("no failures, {} tested", count);
            std::process::exit(1);
        } else {
            println!("{} failures, of the {} files tested", failures, count);
            std::process::exit(2);
        }
    }
}
