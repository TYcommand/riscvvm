#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
#[repr(transparent)]
/// A regular 64-bit register
pub struct Register(pub u8);
impl std::ops::Deref for Register {
    type Target = u8;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl std::ops::DerefMut for Register {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl Register {
    /// Hard-wired zero
    pub const ZERO: Register = Register(0);
    /// Return address | Saver: `caller`
    pub const RA: Register = Register(1);
    /// Stack pointer | Saver: `callee`
    pub const SP: Register = Register(2);
    /// Global pointer
    pub const GP: Register = Register(3);
    /// Thread pointer
    pub const TP: Register = Register(4);
    /// alternate link register | Temporary | Saver: `Caller`
    pub const T0: Register = Register(5);
    /// Temporary | Saver: `Caller`
    pub const T1: Register = Register(6);
    /// Temporary | Saver: `Caller`
    pub const T2: Register = Register(7);
    /// Saved register | Saver: `Callee` | S0 == [FP]
    pub const S0: Register = Register(8);
    /// Saved register | Saver: `Callee`
    pub const S1: Register = Register(9);
    /// Return values | Function arguments | Saver: `Caller`
    pub const A0: Register = Register(10);
    /// Return values | Function arguments | Saver: `Caller`
    pub const A1: Register = Register(11);
    /// Function arguments | Saver: `Caller`
    pub const A2: Register = Register(12);
    /// Function arguments | Saver: `Caller`
    pub const A3: Register = Register(13);
    /// Function arguments | Saver: `Caller`
    pub const A4: Register = Register(14);
    /// Function arguments | Saver: `Caller`
    pub const A5: Register = Register(15);
    /// Function arguments | Saver: `Caller`
    pub const A6: Register = Register(16);
    /// Function arguments | Saver: `Caller`
    pub const A7: Register = Register(17);
    /// Saved register | Saver: `Callee`
    pub const S2: Register = Register(18);
    /// Saved register | Saver: `Callee`
    pub const S3: Register = Register(19);
    /// Saved register | Saver: `Callee`
    pub const S4: Register = Register(20);
    /// Saved register | Saver: `Callee`
    pub const S5: Register = Register(21);
    /// Saved register | Saver: `Callee`
    pub const S6: Register = Register(22);
    /// Saved register | Saver: `Callee`
    pub const S7: Register = Register(23);
    /// Saved register | Saver: `Callee`
    pub const S8: Register = Register(24);
    /// Saved register | Saver: `Callee`
    pub const S9: Register = Register(25);
    /// Saved register | Saver: `Callee`
    pub const S10: Register = Register(26);
    /// Saved register | Saver: `Callee`
    pub const S11: Register = Register(27);
    /// Temporary | Saver: `Caller`
    pub const T3: Register = Register(28);
    /// Temporary | Saver: `Caller`
    pub const T4: Register = Register(29);
    /// Temporary | Saver: `Caller`
    pub const T5: Register = Register(30);
    /// Temporary | Saver: `Caller`
    pub const T6: Register = Register(31);

    /// frame pointer | Saver: `Callee` | FP == [S0]
    pub const FP: Register = Register(8);

    pub const fn name(&self) -> &'static str {
        match self.0 & 0b11111 {
            0 => "ZERO",
            1 => "RA",
            2 => "SP",
            3 => "GP",
            4 => "TP",
            5 => "T0",
            6 => "T1",
            7 => "T2",
            8 => "S0",
            9 => "S1",
            10 => "A0",
            11 => "A1",
            12 => "A2",
            13 => "A3",
            14 => "A4",
            15 => "A5",
            16 => "A6",
            17 => "A7",
            18 => "S2",
            19 => "S3",
            20 => "S4",
            21 => "S5",
            22 => "S6",
            23 => "S7",
            24 => "S8",
            25 => "S9",
            26 => "S10",
            27 => "S11",
            28 => "T3",
            29 => "T4",
            30 => "T5",
            31 => "T6",
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
#[repr(transparent)]
/// A floating point register
pub struct FloatRegister(pub u8);
impl std::ops::Deref for FloatRegister {
    type Target = u8;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl std::ops::DerefMut for FloatRegister {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl FloatRegister {
    /// Temporary | Saver: `Caller`
    pub const FT0: FloatRegister = FloatRegister(0);
    /// Temporary | Saver: `Caller`
    pub const FT1: FloatRegister = FloatRegister(1);
    /// Temporary | Saver: `Caller`
    pub const FT2: FloatRegister = FloatRegister(2);
    /// Temporary | Saver: `Caller`
    pub const FT3: FloatRegister = FloatRegister(3);
    /// Temporary | Saver: `Caller`
    pub const FT4: FloatRegister = FloatRegister(4);
    /// Temporary | Saver: `Caller`
    pub const FT5: FloatRegister = FloatRegister(5);
    /// Temporary | Saver: `Caller`
    pub const FT6: FloatRegister = FloatRegister(6);
    /// Temporary | Saver: `Caller`
    pub const FT7: FloatRegister = FloatRegister(7);
    /// Saved register | Saver: `Callee`
    pub const FS0: FloatRegister = FloatRegister(8);
    /// Saved register | Saver: `Callee`
    pub const FS1: FloatRegister = FloatRegister(9);
    /// Return values | Arguments | Saver: `Caller`
    pub const FA0: FloatRegister = FloatRegister(10);
    /// Return values | Arguments | Saver: `Caller`
    pub const FA1: FloatRegister = FloatRegister(11);
    /// Arguments | Saver: `Caller`
    pub const FA2: FloatRegister = FloatRegister(12);
    /// Arguments | Saver: `Caller`
    pub const FA3: FloatRegister = FloatRegister(13);
    /// Arguments | Saver: `Caller`
    pub const FA4: FloatRegister = FloatRegister(14);
    /// Arguments | Saver: `Caller`
    pub const FA5: FloatRegister = FloatRegister(15);
    /// Arguments | Saver: `Caller`
    pub const FA6: FloatRegister = FloatRegister(16);
    /// Arguments | Saver: `Caller`
    pub const FA7: FloatRegister = FloatRegister(17);
    /// Saved register | Saver: `Callee`
    pub const FS2: FloatRegister = FloatRegister(18);
    /// Saved register | Saver: `Callee`
    pub const FS3: FloatRegister = FloatRegister(19);
    /// Saved register | Saver: `Callee`
    pub const FS4: FloatRegister = FloatRegister(20);
    /// Saved register | Saver: `Callee`
    pub const FS5: FloatRegister = FloatRegister(21);
    /// Saved register | Saver: `Callee`
    pub const FS6: FloatRegister = FloatRegister(22);
    /// Saved register | Saver: `Callee`
    pub const FS7: FloatRegister = FloatRegister(23);
    /// Saved register | Saver: `Callee`
    pub const FS8: FloatRegister = FloatRegister(24);
    /// Saved register | Saver: `Callee`
    pub const FS9: FloatRegister = FloatRegister(25);
    /// Saved register | Saver: `Callee`
    pub const FS10: FloatRegister = FloatRegister(26);
    /// Saved register | Saver: `Callee`
    pub const FS11: FloatRegister = FloatRegister(27);
    /// Temporary | Saver: `Caller`
    pub const FT8: FloatRegister = FloatRegister(28);
    /// Temporary | Saver: `Caller`
    pub const FT9: FloatRegister = FloatRegister(29);
    /// Temporary | Saver: `Caller`
    pub const FT10: FloatRegister = FloatRegister(30);
    /// Temporary | Saver: `Caller`
    pub const FT11: FloatRegister = FloatRegister(31);
}

impl From<Register> for FloatRegister {
    fn from(value: Register) -> Self {
        Self(value.0)
    }
}
impl From<FloatRegister> for Register {
    fn from(value: FloatRegister) -> Self {
        Self(value.0)
    }
}
