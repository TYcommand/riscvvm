//! The riscv tests from [`https://github.com/riscv-software-src/riscv-tests`]
//! in compiled form (as ELF files) are placed in `./compiled/*`. These can
//! be loaded into the VM memory using the libraries defined in this file.

use riscvvm::memory::{map_const::OffsetDualMap, LinearMemory};
pub use riscvvm::{Memory, RiscV, *};

pub fn tests() -> &'static std::path::Path {
    std::path::Path::new("./tests/official/compiled/")
}

pub fn create_memory(
    text: &[u8],
    text_addr: u64,
    data: &[u8],
    data_addr: u64,
) -> OffsetDualMap<LinearMemory, LinearMemory> {
    let mem_text = LinearMemory::new_from_slice(text).offset(text_addr);
    let mem_data = if !data.is_empty() {
        LinearMemory::new_from_slice(data).offset(data_addr)
    } else {
        LinearMemory::new(4096 * 4).offset(text_addr + text.len() as u64)
    };

    (mem_text, mem_data).into()
}

pub fn create_memory_from_elf(bytes: &[u8]) -> (OffsetDualMap<LinearMemory, LinearMemory>, u64) {
    let file = elf::ElfBytes::<elf::endian::LittleEndian>::minimal_parse(bytes).unwrap();

    let text_header = file.section_header_by_name(".text.init").unwrap().unwrap();
    let text = file.section_data(&text_header).unwrap().0;
    let text_addr = text_header.sh_addr;

    let data;
    let data_addr;

    if let Some(data_header) = file.section_header_by_name(".data").unwrap() {
        data_addr = data_header.sh_addr;
        data = file.section_data(&data_header).unwrap().0;
    } else {
        data_addr = 0;
        data = &[];
    };

    (create_memory(text, text_addr, data, data_addr), text_addr)
}

pub fn create_vm(path: &std::path::Path) -> RiscV<OffsetDualMap<LinearMemory, LinearMemory>> {
    let file = std::fs::read(path).unwrap();
    let (mem, pc) = create_memory_from_elf(&file);
    RiscV::new(mem, 1, pc)
}

pub fn run<M: Memory>(mut vm: RiscV<M>) -> Result<(), ()> {
    for core in vm.cores.iter_mut() {
        for reg in core.regs.iter_mut().skip(1) {
            *reg = 0x5454545454545454;
        }
    }

    loop {
        let mut map = mapping::Translation {
            mem: &mut vm.memory,
            privilege: vm.cores[0].csr.privilege(),
            satp: vm.cores[0].csr.satp(),
        };

        let decode = decoder::decode(vm.cores[0].pc, &mut map);

        if let Ok(riscvvm::decoder::Instruction::I(riscvvm::decoder::InstructionI::ECall)) = decode
        {
            if vm.cores[0].get_reg(Register::A0) == 0 {
                return Ok(());
            } else {
                return Err(());
            }
        }

        vm.run();
    }
}

#[macro_export]
macro_rules! test {
    ($name: ident: $file: literal) => {
        #[test]
        fn $name() -> Result<(), ()> {
            run(create_vm(&tests().join($file)))
        }
    };
}
